---
title: "Das Wunder vom Maschsee"
date: 2022-09-04T20:16:08+02:00
draft: false
toc: false
images:
tags:
  - 1. Bundesliga
  - Rennen
  - Klassenerhalt
---

![Team im Ziel](/img/2022-09-03-Hannover-Team-Ziel.jpg "Foto: Tom Gorges")

<p><span class="inactive"><small>von Peter Schwesinger</small></span></p>

"Wir werden bis zur letzten Sekunde alles für den Klassenerhalt geben. Die Jungs können das!"  
  
Gesagt, getan! Das **TEAM WEIMARER INGENIEURE** bleibt nach einem furiosen Saisonfinale, was
kaum dramatischer hätte verlaufen können, erstklassig. Die Marschroute war von Beginn an klar,
unsere Chance lag nur in der Flucht nach vorn.  
Unser erstes Starterpaar, vertreten durch Alexander Kull und Marcel Lehmberg, erwischte einen
optimalen Start beim Schwimmen. Zusammen mit einer größeren Spitzengruppe stiegen sie aufs Rad,
welche bis zum Laufen zusammenblieb. Der ursprüngliche Plan, mit beiden Startern an das zweite
Paar zu übergeben, schien aufzugehen. Die Rechnung hatten wir aber nicht mit Alex' Laufperformance
gemacht. Getragen von den Zuschauern und Betreuern, packte er den wohl fulminantesten Lauf der
Vereinsgeschichte hin. Zwischenzeitlich führend und zum Ende fast gleichauf mit den
Nationalmannschaftsathleten Jonas Schomburg und Lasse Lührs, übergab er an dritter Position
liegend an John Heiland und Theo Sonnenberg.  
Marcel lieferte einen großen Kampf und holte alles aus sich raus, um den Anschluss zu halten.
Die Teamleitung reagierte jedoch blitzschnell und entschied, dass bereits beim Eintreffen von
Alex in der Wechselzone, unser zweites Paar der 2x2 Staffel ins Rennen zu schicken. Das bedeutete
jedoch auch, dass John und Theo beide ins Ziel und in die Wertung kommen mussten. 
Den ordentlichen Vorsprung vor den Verfolgern konnten die Beiden im Wasser zwar nicht gänzlich
verteidigen, allerdings befanden sie sich ebenfalls in der Spitzengruppe auf dem Rad. Einzig
allein das führende Team aus Buschhütten war enteilt.  
Auch hier blieben die Abstände auf der Radstrecke konstant, sodass John und Theo ihre große Stärke
beim Laufen endlich einmal ganz vorn in der 1. Bundesliga zeigen konnten. Gemeinsam liefen sie ein
grandioses Finale. Angepeitscht von unseren Betreuern, Fans und zahlreichen weiteren Sportlern aus
Thüringen, erreichten wir einen unfassbaren vierten Platz! Mit der Erkenntnis, dass dieses Ergebnis
für den Klassenverbleib reichen würde, brach grenzenloser Jubel im Weimarer Lager aus. So etwas
haben wir noch nie erlebt. Das Wunder vom Maschsee!

[Mannschaftsergebnis](https://www.triathlonbundesliga.de/sites/default/files/documents/2022-09/Hannover%20Teamwertung%20M%C3%A4nner.pdf)  
[Ligatabelle](http://www.triathlonbundesliga.de/ergebnisse)  

