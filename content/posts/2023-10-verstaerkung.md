---
title: "Verstärkung für TEAM WEIMARER INGENIEURE"
date: 2023-10-17T17:01:40+02:00
draft: false
toc: false
images:
tags:
  - team
---

![2023-10-17-Ali.jpg](/img/2023-10-17-Ali.jpg)

Die Mannschaftsleitung des **TEAM WEIMARER INGENIEURE** hat sich mit dem Ausklingen der
Wettkampfsaison 2023 verstärkt. Seit Anfang Oktober arbeitet **Aljoscha Willgosch** im
Bereich des Managements aktiv in der Teamleitung mit. \
Als ehemaliger Athlet mit zahlreichen Einsätzen in der 1. wie auch 2. Triathlon-Bundesliga
ist Aljoscha mit seiner langjährigen, sportlichen Erfahrung eine Bereicherung für die
Mannschaft. Für seine Arbeit im **TEAM WEIMARER INGENIEURE** wünschen wir ihm alles Gute!
