---
title: "Team IngWE weiter im Aufwind"
date: 2024-08-06T17:47:58+02:00
draft: false
toc: false
images:
tags:
  - Rennen
  - 2. Bundesliga Nord
---

![Ziel Struan Bennet](/img/2024-08-04-Struan-Ziel.jpg)

Beim vorletzten Wettkampf der Zweitligasaison am vergangenen Wochenende
in Salzgitter errang das **TEAM WEIMARER INGENIEURE** erneut einen
Platz auf dem Podium.  
Nach dem Tagessieg beim [Heimrennen](/posts/2024/06/triathlonfest-in-weimar),
dem [Kulturstadttriathlon](https://kulturstadttriathlon.de), erkämpften
Struan Bennet (1.), Leon Fischer (16.), John Heiland (23.), Peter
Lehmann (32.) und Philipp Leiteritz (41.) den dritten Rang in der
Mannschaftswertung.  

Nach langwieriger Anreise im Vorfeld des Rennens gelang Neuzugang
Struan Bennet auf Anhieb der Sprung auf den höchsten Podestplatz. Mit
Theo Sonnenberg in [Eutin](/posts/2024/06/theo-siegt-in-eutin/) sowie
Alexander Kull in [Weimar](/posts/2024/06/triathlonfest-in-weimar/)
stellte das **TEAM WEIMARER INGENIEURE** in dieser Saison somit zum
dritten Mal den Tagessieger der Einzelwertung.  

Mit diesen Ergebnissen setzt das **TEAM WEIMARER INGENIEURE** seine
beim [Kulturstadttriathlon](https://kulturstadttriathlon.de)
eingeleitete Erfolgsserie fort und blickt voller Vorfreude auf das
kommende Rennen in der 1. Triathlon-Bundesliga, dass am Abend des
kommenden Samstags (10. August) im Herzen von Dresden stattfinden
wird.  
In Dresden wird nochmals höchste Konzentration gefragt sein, um
wichtige Punkte zum angestrebten Top-10 Platz der Gesamtwertung zu
sammeln.  

[Einzelergebnis](https://my1.raceresult.com/274448/RRPublish/data/pdf?name=F%20Bundesliga2%20Nord%7C2a%20Ergs%20TBL2%20Einzel%20M%C3%A4nner&contest=4&lang=de)  
[Mannschaftsergebnis](https://my1.raceresult.com/274448/RRPublish/data/pdf?name=F%20Bundesliga2%20Nord%7C3a%20Ergs%20TBL2%20Teams%20M%C3%A4nner&contest=4&lang=de)  
[Scoreboard](https://www.ingweultras.com/scoreboard/)
