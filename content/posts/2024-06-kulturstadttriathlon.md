---
title: "Triathlonfest in Weimar"
date: 2024-06-27T13:44:16+02:00
draft: false
toc: false
images:
tags:
  - Rennen
  - 2. Bundesliga Nord
---

<figure style="margin: auto">
    <img src="/img/2024-06-23-KST-Weimar.jpg" alt="Team">
    <figcaption style="text-align: right">Foto: Tom Gorges</figcaption>
</figure>

Im Rahmen des [Kulturstadttriathlon](https://kulturstadttriathlon.de/) veranstaltete die
Triathlon-Abteilung des HSV Weimar e.V. am vergangenen Sonntag ein echtes Triathlonfest.
Neben Nachwuchswettkämpfen, Jedermannrennen und diversen Staffelformaten machte die 2. 
Triathlonbundesliga Nord mit einem Sonderformat Station Weimar. Ein Minitriathlon (250m
Schwimmen, 6km Radfahren, 1.5km Laufen) musste als Viererstaffel von jedem Teammitglied
absolviert werden.   

In einem sehr dramatischen Rennverlauf wusste das **TEAM WEIMARER INGENIEURE** den Vorteil
durch das Weimarer Publikum zu nutzen und zeigte mit einem schlussendlich souveränen
Heimsieg, dass es auch 2024 zu den Siegkandidaten in der 2. Bundesliga zählt.  
Zunächst musste Richard Feuer einem überaus hektischen Schwimmstart und den anschließenden
Positionskämpfen im Wasser Tribut zollen. Mit einer beherzten Rad- und Laufleistung hielt
er die Staffel des **TEAM WEIMARER INGENIEURE** jedoch in Schlagdistanz zur Spitze. Mit
einem hervorragenden Rennen schaffte es Leon Fischer den Anschluss an die Spitzengruppe
herzustellen, bevor er Philipp Mack ins Rennen schickte. Dieser wiederum setzte die
Vorgaben des sportlichen Leiters Tom Eismann brilliant um und attackierte auf dem Rad, um
den letzten Starter des Teams Alexander Kull mit einem Vorsprung auszustatten. Alexander
baute den etwa 10-sekündigen Vorsprung sowohl auf der Schwimm- als auch der Radstrecke
weiter aus und lief unter dem Jubel der Weimarer Triathlongemeinschaft mit komfortablem
Vorsprung ins Ziel.  

Medial wurde das Rennen von einem [Live-Stream](https://www.youtube.com/watch?v=EnUL8zPRm_k)
sowie einem Kamerateam des MDR begleitet, dass einen entsprechenden
[Fernsehbeitrag im MDR Thüringenjournal](https://www.mdr.de/video/mdr-videos/f/video-835386.html)
produzierte.  
Teammanager Aljoscha Willgosch ließ im Nachgang das Rennen bei
[Radio Lotte](https://www.radiolotte.de/radio/sportplatz-am-mittwoch-alles-ausser-fussball-vom-26.-juni-42445.html)
in einem Interview Revue passieren.  


[Mannschaftsergebnis 2. Triathlon Bundesliga Nord](https://my4.raceresult.com/283471/RRPublish/data/pdf?name=02%3A%20Ergebnislisten%7C12%20Teamwertung%20BL%20M%C3%A4nner&contest=12&lang=en)

[Ergebnisse 2. Triathlon Bundesliga Nord](https://www.ingweultras.com/scoreboard/)

