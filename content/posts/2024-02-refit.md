---
title: "Exklusivpartnerschaft mit Refit"
date: 2024-02-10T12:51:44+01:00
draft: false
toc: false
images:
tags:
  - sponsoring
---

<a class="unstyled" href="https://www.refit-jena.de">
<img style="margin: auto" src="/img/refit_Logo_original_0916.png" width="400px" alt="ReFit Therapie und Training" />
</a>

Das **TEAM WEIMARER INGENIEURE** freut sich über eine neue Kooperation im Bereich Gesundheit.  
Am vergangenen Mittwoch besuchten Mitglieder des Teams sowie des Vereins die Trainings- und Behandlungsräumlichkeiten
von [refit - Therapie & Training](https://www.refit-jena.de) in Jena. Matthias Passarge, Inhaber der Physio- und
Osteopathiepraxis, führte die Athleten durch seine Praxis und stellte das breite Portfolio der Behandlungs- und
Trainingsmöglichkeiten vor, von denen die Sportler des Teams zukünftig profitieren dürfen. Auch die Teamleitung und
insbesondere Trainer Tom Eismann war von den vielseitigen präventiven wie rehabilitativen Angeboten begeistert, die
der Mannschaft trainingsbegleitend zur Verfügung stehen.

![Unterzeichnung Kooperationsvereinbarung](/img/2024-02-Refit.jpg "Foto: Jürgen Scheere")

Mit Unterzeichnung der Kooperationsvereinbarung ist [refit - Therapie & Training](https://www.refit-jena.de) nun
offizieller [Exklusivpartner Gesundheit](/sponsoring) des **TEAM WEIMARER INGENIEURE**.

