---
title: "Saisonabschluss 2024"
date: 2024-10-22T19:50:13+02:00
draft: false
toc: false
images:
tags:
  - sponsoring
---

<figure style="margin: auto">
    <img src="/img/2024-10-22-Saisonabschluss-2024.jpg" alt="Saisonabschluss">
    <figcaption style="text-align: right">Foto: Jeremias Heinig</figcaption>
</figure>

Siebzig geladene Gäste kamen zu unserem Saisonabschluss ins MonAmi Weimar. Nach einleitenden
Worten durch den Hauptgeschäftsführer des Thüringer Landessportbundes Thomas Zirkel wurde
die Mannschaft für ihre Errungenschaften in 2024 gefeiert.  
  
Das Highlight neben dem Catering und der guten Laune war eine besondere Veröffentlichung:

Der Kulturstadttriathlon Weimar geht bereits im nächsten Jahr in die dritte Runde. Am 22.
Juni 2025 wird die 2. Triathlonbundesliga Nord wieder bei uns zu Gast sein und wir werden
sie im Weimarer Schwanseebad mit offenen Armen empfangen.
