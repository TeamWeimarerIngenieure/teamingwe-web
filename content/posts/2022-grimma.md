---
title: "Heimspiel in Grimma"
date: 2022-07-18T15:48:20+02:00
draft: false
toc: false
author: "Peter Schwesinger"
images:
tags:
  - 2. Bundesliga Nord
  - Rennen
---

![Mannschaft Siegerehrung](/img/2022-07-17-Grimma-Mannschaft-Siegerehrung.jpg "Foto: Tom Gorges")

<p><span class="inactive"><small>von Peter Schwesinger</small></span></p>

Seit vielen Jahren fahren wir regelmäßig nach Grimma, um beim Muldental-Triathlon
dabei zu sein. Wir haben festgestellt, es ist immer wieder schön dort! Das lag
vergangenes Wochenende nicht nur an der Performance unseres Teams, sondern wieder
einmal an der wunderbaren Kulisse im Muldental.  

Nachdem vergangenes Jahr nur ein Duathlon stattfinden konnte, stürzten sich die
Athleten der 2. Bundesliga Nord am Sonntag wieder ins kühle Nass. Das **Team Weimarer
Ingenieure** kam dabei am Start sehr gut weg, jedoch trügte der Schein von außen
betrachtet zunächst ein wenig.  
Felix Günther kassierte an der Boje Schläge ins Gesicht. Dabei verlor er nicht nur
seine Schwimmbrille, sondern auch seinen Rhythmus und letztendlich sein Rennen. Er
stieg vernünftigerweise an der ersten Radwende mit Schwindelgefühlen und rasendem
Puls aus. Hochmotiviert wollte er sein Ergebnis aus Verl für das Team verbessern,
nun musste das Team für ihn geradestehen. Kopf hoch Felix, was uns nicht umhaut,
macht uns nur stärker!  
Der Druck stieg dadurch besonders auf Ricardo Ammarell als Viertplatzierten des Teams,
da er nun zwingend das Ziel erreichen musste. An der Spitze machte unser Youngster
Leon Fischer da weiter, wo er in Berlin aufgehört hatte: Mit beherztem Einsatz kämpfte
er sich zunächst in die erste Radgruppe und erarbeitete sich beim Laufen einen tollen 14.
Platz.  
Peter Lehmann und unser zweiter Youngster Aaron Hüter leisteten in der Verfolgergruppe
gute Arbeit und zeigten beim Laufen dann ihre Klasse. Peter lief als unser Stärkster auf
Platz 8 ein, während Aaron knapp dahinter als 10. folgte und wieder ein Top-Ergebnis
erzielte.  
Ricardo (44.), der nicht seinen besten Tag erwischte, holte alles aus sich raus und
sicherte uns den starken zweiten Platz im Teamergebnis. Unterm Strich stand ein
erfolgreicher Tag mit einer beeindruckenden Teamleistung auch neben der Strecke. 

Die Ergebnisse gibt es unter:
* [Einzelwertung](http://www.triathlonbundesliga.de/sites/default/files/documents/2022-07/Ergebnisliste_2._Bundesliga_M%C3%A4nner_Einzel.pdf)
* [Mannschaftswertung](http://www.triathlonbundesliga.de/sites/default/files/documents/2022-07/Cupliste_2._Bundesliga_M%C3%A4nner_Team.pdf)

