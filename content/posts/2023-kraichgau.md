---
title: "Starker Saisonauftakt"
date: 2023-05-23T17:07:30+02:00
draft: false
toc: false
images:
tags:
  - 1. Bundesliga
  - Rennen
---

{{< rawhtml >}} 

<video width=100% controls autoplay muted>
    <source src="/videos/2023-05-20-Kraichgau-Start.mp4" type="video/mp4">
    Your browser does not support the video tag.  
</video>

{{< /rawhtml >}}

Zum Saisonauftakt der 1. Bitburger 0.0% Triathlon-Bundesliga präsentierte sich das **TEAM WEIMARER INGENIEURE** in bester Verfassung.
Nachdem sich die Mannschaft wenige Tage vor dem Rennen im Kraichgau beim Hauptsponsor
[Glatt Ingenieurtechnik Weimar](https://www.glatt.com) hoffnungsvoll und in bester Laune präsentierte, musste die
geplante Aufstellung für den Ligastart kurzfristig umgebaut werden. Krankheitsbedingt fiel Leistungsträger Marcel
Lehmberg am Vortag des Rennens aus, sodass Aaron Hüter in das Starter-Quartett für das Rennen im 2x2 Staffelformat
aufrückte.  
Mit Alexander Kull und Richard Feuer startete das **TEAM WEIMARER INGENIEURE** beherzt in die Liga-Saison 2023. Beide
Sportler sicherten sich direkt einen Platz in der Spitzengruppe und legten so den Grundstein für ein erfolgreiches
Rennwochenende. Alexander Kull spielte wie zuletzt beim [Liga-Finale in Hannover](http://team-weimarer-ingenieure.de/posts/2022/09/das-wunder-vom-maschsee/)
seine Laufstärke aus und schickte das zweite Starterpaar bestehend aus John Heiland und Aaron Hüter auf einer
aussichtsreichen Position liegend ins Rennen.  
Auf der 375m langen Schwimmstrecke mussten die beiden dem hohen Tempo der starken Schwimmer der Teams aus Buschhütten 
und Saarbrücken Tribut zollen. Mit einem Kraftakt beim Ausstieg aus dem Hardtsee und einem schnellen Wechsel führten
beide die Verfolgergruppe an.

![Mannschaft](/img/2023-05-17-Mannschaft.jpg "Mannschaftspräsentation{{< rawhtml >}}<br/>{{< /rawhtml >}}
hinten v.l.n.r: Julius Domnick, Henry Beck, Leon Fischer, Aaron Hüter, Richard Feuer  
{{< rawhtml >}}<br/>{{< /rawhtml >}}vorne v.l.n.r: Mattis Haselbach, Marcel Lehmberg, Philipp Leiteritz, Theo Sonnenberg")

Sowohl auf dem Rad als auch auf der Laufstrecke arbeiteten John und Aaron hervorragend zusammen, sodass sie das Rennen
ungefährdet auf Platz 10 beendeten. Mit diesem Ergebnis legte das Quartett die Basis für den Klassenerhalt und eine
hoffentlich etwas weniger aufregende Saison als 2022. Im vergangenen Jahr musste das **TEAM WEIMARER INGENIEURE** bis
zum letzten Wettkampf im Abstiegskampf zittern.

[Mannschaftsergebnis](https://www.triathlonbundesliga.de/sites/default/files/documents/2023-05/Ergebnisse%20Kraichgau%20M%C3%A4nner.pdf)  
[Ligatabelle](https://my1.raceresult.com/240724/RRPublish/data/pdf?name=10%20Ergebnislisten%7C00%20Ergebnisliste%201.%20Bundesliga&contest=1&lang=de)
