---
title: "Tübinger Bann gebrochen"
date: 2024-07-21T18:48:13+02:00
draft: true
toc: false
images:
tags:
  - Rennen
  - 1. Bundesliga
---

<figure style="margin: auto">
    <img src="/img/2024-07-tuebingen-1.jpg" alt="Alexander Kull">
    <figcaption style="text-align: right">Foto: Tom Gorges</figcaption>
</figure>

Nach einem katastrophalen Rennen in Tübingen im Jahr 2019 und einem
[folgenschweren Missgeschick](/posts/2023/07/verflixtes-t%C3%BCbingen/)
im letzten Jahr konnte das **TEAM WEIMARER INGENIEURE** beim heutigen
Wettkampf der 1. Bitburger 0.0% Triathlon Bundesliga mit durchweg
stabilen Leistungen einen wichtigen 10. Platz einfahren und damit seiner
Tübinger Pechsträhne eine Ende setzen.  

<figure style="margin: auto">
    <img src="/img/2024-07-tuebingen-6.jpg" alt="Philipp Mack">
    <figcaption style="text-align: right">Foto: Tom Gorges</figcaption>
</figure>

Das Quartett um Alexander Kull (12.), Philipp Mack (30.), Theo Sonnenberg
(32.) und Richard Feuer (38.) schaffte es, den beim
[Kulturstadttriathlon](https://kulturstadttriathlon.de/) aufgenommenen
Schwung in das zweite Rennen der 1. Liga mitzunehmen, um nach dem
holprigen Start Anfang Mai im Kraichgau Boden auf die Abstiegsplätze gut
zu machen.  
Mit diesem soliden Ergebnis findet sich das **TEAM WEIMARER INGENIEURE**
auf Rang 12 der Ligawertung ein, bevor am 10. August das dritte
Saisonrennen in Dresden stattfindet.


### Links

[Ergebnisse Einzel](https://www.triathlonbundesliga.de/sites/default/files/documents/2024-07/M%C3%A4nner%20Einzelwertung.pdf)  
[Ergebnisse Mannschaft](https://www.triathlonbundesliga.de/sites/default/files/documents/2024-07/M%C3%A4nner%20Teamwertung.pdf)  
[Scoreboard](https://www.ingweultras.com/scoreboard/)  

