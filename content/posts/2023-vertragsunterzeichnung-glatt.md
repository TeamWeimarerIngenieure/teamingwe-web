---
title: "Fortsetzung einer langjährigen Zusammenarbeit"
date: 2023-03-31T19:31:23+02:00
draft: false
toc: false
images:
  - /img/Glatt-space.jpg
tags:
  - sponsoring
---

![Vertragsunterzeichnung](/img/2023-03-30-Vertragsunterzeichnung-Glatt.jpg)

Am gestrigen Donnerstag trafen sich die Verantwortlichen des **TEAM WEIMARER INGENIEURE**
mit der Geschäftsführung der [Glatt Ingenieurtechnik GmbH](https://www.glatt.com/)
Weimar. Erfreulicher Anlass der Zusammenkunft war die gemeinsame Unterzeichnung der
Sponsoringvereinbarung für die kommende Triathlon-Saison. Somit bleibt Glatt auch 2023
Hauptsponsor des Teams.  

[![Glatt](/img/Glatt-space-flat.jpg)](https://www.glatt.com/)

Das Engagement der [Glatt Ingenieurtechnik GmbH](https://www.glatt.com/) reicht zurück
bis in das Jahr 2008, in dem das noch junge **TEAM WEIMARER INGENIEURE** in der Triathlon
Regionalliga Ost erste Erfolge feierte. Neben dem formalen Akt der Vertragsunterzeichnung
markierte dieser wichtige Termin somit auch das 15-jährige Jubiläum der Zusammenarbeit.
Ohne diese langjährige Unterstützung hätte die Entwicklung der Mannschaft hin zu einem
Erst- und Zweitligateam kaum verwirklicht werden können. Aus diesem Grund sind wir der
[Glatt Ingenieurtechnik GmbH](https://www.glatt.com/) zu tiefstem Dank verpflichtet.

