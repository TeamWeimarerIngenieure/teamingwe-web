---
title: "John Heiland über sein Erstligarennen in Hannover"
date: 2023-09-06T16:47:25+02:00
draft: false
toc: false
images:
tags:
  - 1. Triathlon Bundesliga
  - 2. Triathlon Bundesliga
  - Rennen
---

![Team Zieleinlauf](/img/2023-09-02-Hannover-Team.jpg "Foto: Tom Gorges")

<p><span class="inactive"><small>von John Heiland</small></span></p>

Am Samstag fand das letzte Rennen der 1. Bitburger 0.0% Triathlon-Bundesliga dieser Saison in Hannover statt. Der Druck war dieses Mal
nicht so groß wie im letzten Jahr, da wir den Klassenerhalt schon in den vorherigen Rennen gesichert hatten. Dieses Jahr
sollte es in Hannover wieder ein Sonderformat geben, bei dem man zuerst in einem Prolog alleine einen ganz kurzen
Triathlon absolviert und nach dann nach einer kurzen Pause mit den Rückständen noch einen Triathlon absolviert.  
  
Der erste Start war erst am späten Nachmittag und der zweite dann noch später. Meine Vorbereitung lief ganz gut und ich
ging sehr entspannt an den Start. Die Athleten starteten aller 20 Sekunden und mein erstes Schwimmen lief irgendwie gar
nicht so gut. Auch wenn es nur 250m waren, hatte ich das Gefühl, dass ab der Hälfte so gar nichts mehr ging und so wurde
ich auch von dem Athleten hinter mir beim Wasserausstieg eingeholt.  
  
Als ich endlich wieder Land unter den Füßen hatte, gab ich direkt Vollgas und konnte wieder eine kleine Lücke nach
hinten aufmachen. Der lange Wechselweg hier in Hannover kommt mir da auch zu Gute. Der Wechsel aufs Rad klappte
reibungslos und von da an fuhr ich nur noch Anschlag.  

Die Wattwerte waren auch ganz ok, jedoch wurde ich kurz vor der Hälfte der Radstrecke dann doch von dem hinter mir
gestarteten Athleten überholt. Auch der zweite Wechsel klappte sehr gut und so ging es weiter mit Vollgas auf die
letzten 1,5 Laufkilometer.  
Das Laufen fühlte sich auch wieder ganz gut an und so beendete ich nach 19 Minuten mit gemischten Gefühlen meinen
Prolog. Am Ende bedeutete das für mich den 47. Platz mit einem Rückstand von 1:48 auf die Spitze. Im Zeitvergleich habe
ich vor allem beim Schwimmen etwas zu viel verloren. Noch war nichts verloren und ich freute mich schon auf den zweiten
Teil. Ich wusste, dass ich beim zweiten Schwimmen nochmal alles geben musste, um überhaupt noch Chancen auf ein
zufriedenstellendes Ergebnis haben zu können.  
  
Dieses Mal hatte ich gleich vom Start weg ein gutes Gefühl und konnte bei den Athleten vor mir dranbleiben.  Ich holte
hier alles raus was ging und am Ende war diese Schwimmzeit im Vergleich zu anderen Athleten plötzlich sehr gut - vorher
habe ich auf 250m 20 Sekunden verloren und jetzt auf 500m 1 Sekunde. Auch hier nutzte ich den langen Wechselweg wieder,
um in die kleine Gruppe kurz vor mir zu kommen. Auch dieser Wechsel klappte perfekt und es ging auf dem Rad direkt
wieder mit Vollgas los. Es dauerte ein wenig bis die kleine Gruppe sich etwas sortiert hatte, aber hier fuhr ich fast
die ganze Zeit am Limit.  
  
Das Ganze ging über zwei Runden so weiter und ich wusste wirklich nicht so recht, wie dann das Laufen noch so klappen
sollte. Auf der dritten Runde konnten wir dann noch die große Gruppe vor uns einholen und dann musste ich erstmal auf
den letzten Kilometern versuchen meinen Puls etwas fürs Laufen runter zu bekommen. Das Radfahren war für mich wirklich
sehr hart und recht unrhythmisch und so hatte ich kaum eine andere Wahl, als ganz am Ende der Gruppe vom Rad zu
steigen.  
  
Die ersten Meter fühlten sich noch nicht gleich flüssig an, aber das Gefühl kam recht schnell wieder und so sammelte ich
nach und nach immer wieder jemanden ein.  
Am Ende bin ich dann 33. geworden und bin mit der Leistung im zweiten Rennen sehr zufrieden. Durch ein sehr starkes
Rennen von Alex und Theo konnten wir uns in der Tageswertung wieder den 4. Platz sichern, einfach der Wahnsinn!  
  
Mit einem 9. Tabellenplatz in der Endabrechnung der 1. Bitburger 0.0% Triathlon-Bundesliga bin ich super happy. Am Sonntag lieferten
unsere Jungs beim Finale der 2. Bundesliga Nord einen 6. Platz ab und sicherten damit im Saisonergebnis Platz 4!

[Mannschaftsergebnis 1. Liga](https://www.triathlonbundesliga.de/sites/default/files/documents/2023-09/1.%20Liga%20M%C3%A4nner%20Team.pdf)  
[Ligatabelle 1. Liga](https://my1.raceresult.com/240724/RRPublish/data/pdf?name=10%20Ergebnislisten%7C00%20Ergebnisliste%201.%20Bundesliga&contest=1&lang=de)

[Mannschaftsergebnis 2. Liga](https://www.triathlonbundesliga.de/sites/default/files/documents/2023-09/2.%20liga%20m%C3%A4nner%20team.pdf)  
[Ligatabelle 2. Liga](https://my1.raceresult.com/240724/RRPublish/data/pdf?name=10%20Ergebnislisten%7C00%20Ergebnisliste%202.%20Bundesliga%20Nord&contest=5&lang=de)  
