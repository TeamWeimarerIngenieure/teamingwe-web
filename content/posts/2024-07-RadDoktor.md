---
title: "Bike-Support durch RadDoktor"
date: 2024-06-29T15:00:30+02:00
draft: false
toc: false
images:
tags:
  - sponsoring
---

<a class="unstyled" href="https://www.rad-doktor.de/">
<img style="margin: auto; width: 500px" src="/img/Logo_RadDoktor_quer.png" alt="Logo RadDoktor"/>
</a>

Das **TEAM WEIMARER INGENIEURE** freut sich die [Rad-Doktor GmbH](https://www.rad-doktor.de) als
Exklusivpartner Bike-Support in den Reihen seiner Partner begrüßen zu dürfen!  
Mit seiner im Zentrum Weimars gelegenen Filiale mit angeschlossener Werkstatt ist der
[Rad-Doktor](https://www.rad-doktor.de) die Anlaufstelle für alle Fragen rund ums Thema Fahrrad.  

Seit der offiziellen Vertragsunterschrift am vergangenen Mittwoch, zu der Vereins- sowie
Abteilungssvertreter, Teammanager Aljoscha Willgosch und Rad-Doktor-Geschäftsführer Patrick Rosenhan
anwesend waren, unterstützt der [Rad-Doktor](https://www.rad-doktor.de) die Athleten des
**TEAM WEIMARER INGENIEURE** zunächst bei der Wartung und Reparatur ihrer Rennmaschinen.  
Darüber hinaus sind für die Kooperation zukünftig weitere gemeinsame Aktivitäten geplant. Man darf also
gespannt bleiben!  
