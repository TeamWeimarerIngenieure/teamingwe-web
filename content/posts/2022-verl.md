---
title: "Verler Doppeldebüt"
date: 2022-07-11T07:24:44+02:00
draft: false
toc: false
images:
tags:
  - 2. Bundesliga Nord
  - Rennen
---

![Pascal Unbehaun Rad](/img/2022-07-10-Verl-Pascal-Rad.jpg)

Nachdem sich das **TEAM WEIMARER INGENIEURE** in den ersten beiden Wettkämpfen
der 2. Bundesliga Nord in den Top-3 der Liga etabliert hatte, war es mit dem
dritten Wettkampf in Verl an der Zeit Pascal Unbehaun und Tilman Kühne erstmalig
Bundesligaluft schnuppern zu lassen. Neben den beiden Debütanten wurde das
Weimarer Aufgebot von Aaron Hüter, Peter Lehmann und Felix Günther komplettiert.  
Während sich Aaron, Felix, Peter und Tilman nach dem Schwimmen in einer größeren
Verfolgergruppe einfanden, startete Schwimm-Newcomer Pascal das Radrennen im
letzten Drittel des Feldes. Dank seiner Radstärke war das Schwimmdefizit aber
schnell korrigiert und Pascal leistete wertvolle Nachführarbeit in der Verfolgung
(siehe Titelbild).  
Beim abschließenden 5km-Lauf spielten insbesondere Aaron und Peter ihre
Lauffähigkeiten aus und liefen auf den Plätzen 8 und 15 ins Ziel. Nicht weit
dahinter folgte Felix mit einem soliden Rennen auf Rang 28.  
Mit Problemen hatte Tilman zu kämpfen, der bis zur Ziellinie seinen Laufrhythmus
am gestrigen Tag beharrlich aber leider vergeblich suchte. Trotz der harten Arbeit
auf dem Rad legte auch Pascal einen ordentlichen Lauf hin und machte zum Ende des
Rennens weitere Plätze gut, bevor er als 60. ins Ziel einlief.  
In der Tagessumme lag das **TEAM WEIMARER INGENIEURE** mit Platz 6 voll und ganz
im Bereich der Erwartungen der Mannschaftsleitung und behauptete sich den dritten
Gesamtrang der Ligatabelle.

Die vollständigen Ergebnisse des Wettkampfes sind unter folgendem Link zu finden:

* [Wertung Verl](http://www.triathlonbundesliga.de/sites/default/files/documents/2022-07/Verl%20M%C3%A4nner.pdf)

