---
title: "Knapper Sprint in Salzgitter"
date: 2023-08-07T19:31:59+02:00
draft: false
toc: false
images:
tags:
  - 2. Bundesliga Nord
  - Rennen
---

![2023-08-06-Salzgitter-Leon.jpg](/img/2023-08-06-Salzgitter-Leon.jpg)

Beim Sprintwettkampf der 2. Triathlon-Bundesliga Nord erkämpfte das **TEAM WEIMARER INGENIEURE** in der Besetzung
Aaron Hüter (14.), Leon Fischer (18.), Felix Günther (27.), Philipp Leiterizt (36.) und Thilo Kühne (51.) einen
soliden 5. Platz in der Mannschaftswertung.  
Mit nur zwei Platzziffern Rückstand auf das Triathlon-Team aus Ratingen verlief auch dieses Rennen wieder denkbar knapp.
Mit diesem Ergebnis verbesserte sich die zweite Mannschaft in der Gesamttabelle auf Rang 4. Damit ist für die
verbleibenden Wettbewerbe in Grimma und in Hannover Spannung garantiert, denn die Tabellenplätze drei bis fünf
trennen gerade einmal fünf Zähler.  
  
Weiter geht es in der 2. Triathlon-Bundesliga Nord mit einem Mannschaftssprint am 20.08.2023 im sächsischen Grimma.  
  
[Mannschaftsergebnis](https://www.triathlonbundesliga.de/sites/default/files/documents/2023-08/Ergebnisse%20M%C3%A4nner.pdf)  
[Ligatabelle](https://my1.raceresult.com/240724/RRPublish/data/pdf?name=10%20Ergebnislisten%7C00%20Ergebnisliste%202.%20Bundesliga%20Nord&contest=5&lang=de)  
