---
title: "Bundesliga-Auftakt 2021"
date: 2021-06-28T14:12:48+02:00
draft: false
toc: false
images:
tags:
  - 2. bundesliga nord
  - rennen
---

![Team Ziel](/img/2021-06-26-Team-Ziel-Potsdam.jpg)

Am vergangenen Wochenende beschickte das **TEAM WEIMARER INGENIEURE** gleich zwei Wettkämpfe, die im Rahmen der Liga-Saison 2021 auf dem Programm standen.

Los ging es am Samstag mit dem Auftaktwettkampf zur 2. Triathlon-Bundesliga Nord in Potsdam, bei dem die Athleten zunächst bei einem Prolog bestehend aus 250m Schwimmen, 2.8km Radfahren und 1100m Laufen allein gegen die Uhr kämpfen mussten. Am Nachmittag folgte ein Staffeltriathlon über ebenso kurze Distanzen bei dem mit den Zeitabständen aus dem Prolog gestartet wurde.
Während Alexander Kull, Aljoscha Willgosch, John Heiland und Marcel Lehmberg nach dem Prolog noch mit wenigen Sekunden Rückstand auf das Berliner Triathlon-Team auf Platz 2 rangierten, gelang es der Mannschaft im Staffelwettbewerb das Rennen zu drehen und einen deutlichen Sieg einzufahren.

Für Felix Günther, Theodor Popp, Pascal Unbehaun und nochmals Marcel Lehmberg galt es im Rahmen der Regionalliga-Ost bei einem weiteren dezentralen Wettkampf einen Sprint-Triathlon zu absolvieren. Marcel, der für den krankheitsbedingt ausgefallenen Theo Sonnenberg kurzfristig einsprang, bewies auch am Tag nach seinem Bundesliga-Einsatz nochmals seine Klasse und holte mit der schnellsten Einzelzeit des Tages den Gesamtsieg. In der Mannschaftswertung musste sich das Quartett nur dem Berliner Triathlon-Team knapp geschlagen geben.
