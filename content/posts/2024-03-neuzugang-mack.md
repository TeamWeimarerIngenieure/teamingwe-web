---
title: "Neuzugang Philipp Mack"
date: 2024-03-04T18:30:00+01:00
draft: false
toc: false
images:
tags:
  - team
---

<figure style="margin: auto">
    <img src="/img/2024-03-Philipp-Mack.jpg" alt="Philipp Mack" width="400px">
    <figcaption style="text-align: right">Foto: Tom Gorges</figcaption>
</figure>

Mit Philipp Mack wird das **TEAM WEIMARER INGENIEURE** in der Saison 2024
[weiter verstärkt](/posts/2024/02/neuzugang-struan-bennet/).  
Philipp durchlief das Talenttransferprogramm der Deutschen Triathlon Union und absolvierte 2019 seinen ersten Triathlon
in Erbach.
Bereits im Jahr 2022 nahm der heute 20-jährige erfolgreich an den Deutschen Meisterschaften der Junioren teil und
belegte Platz 7. Ein Jahr später folgten die ersten Einsätze bei Continental-Cups im Elitebereich.  
Der rasante sportliche Aufstieg unterstreicht die Ambitionen des gebürtigen Ulmers und den Hunger auf Erfolge in der 1.
Bitburger 0.0% Triathlon-Bundesliga.  
Auf der Suche nach einer langfristigen, sportlichen Perspektive freut sich Philipp mit dem **TEAM WEIMARER INGENIEURE**
eine Mannschaft gefunden zu haben, die ein kompetitives und gleichzeitig herzliches Umfeld bietet.
