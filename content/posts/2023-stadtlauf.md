---
title: "IngWE-Athleten beim Weimarer Stadtlauf erfolgreich"
date: 2023-10-15T16:35:28+02:00
draft: false
toc: false
images:
tags:
  - Rennen
---

![2023-10-14-Stadtlauf.jpg](/img/2023-10-14-Stadtlauf.jpg)

Am vergangenen Samstag wurde der [Weimarer Stadtlauf](https://weimarer-stadtlauf.de/),
organisiert vom [HSV Weimar e.V.](https://www.hsv-weimar.de), zum 32. Mal ausgetragen.
Zum traditionellen Zwiebelmarkt trafen sich auch in diesem Jahr mehr als 1.300 Läufer
aus über 20 Nationen. Auch das **TEAM WEIMARER INGENIEURE** schickte eine kleine
Läuferdelegation ins Heimrennen. \
 \
Mit Richard Feuer (3.) und Aaron Hüter (10.) landeten gleich zwei Athleten des Teams
unter den besten Zehn des Hauptlaufes über 11km. Mit einer Zeit von 38:18 Minuten
musste sich Richard nur dem Sieger Samson Tesfazghi Hayalu (SV Sömmerda) sowie Dejen
Ayele (Team Sächsischer Hof) geschlagen geben. \
 \
Sowohl Aaron (41:17 Minuten; U20) als auch Richard (U23) konnten mit ihren gelaufenen
Zeiten Siege in ihren Altersklassen feiern.

[Ergebnisliste](https://my.raceresult.com/240433/)
