---
title: "Team-Tag mit Refit"
date: 2025-02-23T20:06:25+01:00
draft: false
toc: false
images:
tags:
  - sponsoring
---

![Unterzeichnung Kooperationsvereinbarung](/img/2025-02-Refit.jpg "Foto: Tom Gorges")

Beim Team-Tag des **TEAM WEIMARER INGENIEURE** bei Gesundheitspartner [refit - Therapie & Training](https://www.refit-jena.de) in Jena
standen gleich mehrere Highlights auf dem Programm. Zu Beginn der noch jungen Triathlonsaison 2025 absolvierten
die  ingWE-Sportler des Teams diverse Kraft- und Mobilitätstests, um trainingsbegleitende Maßnahmen zur Regeneration
und Verletzungsprophylaxe künftig noch zielgerichteter auf die individuellen Bedürfnisse jedes einzelnen Athleten
ausrichten zu können.

<a class="unstyled" href="https://www.refit-jena.de">
<img style="margin: auto" src="/img/refit_Logo_original_0916.png" width="400px" alt="ReFit Therapie und Training" />
</a>

Mit Unterzeichnung der Kooperationsvereinbarung bleibt [refit - Therapie & Training](https://www.refit-jena.de) für weitere drei Jahre
offizieller [Exklusivpartner Gesundheit](/sponsoring) des **TEAM WEIMARER INGENIEURE**. Mit dieser langfristigen Partnerschaft
sind unsere Sportler nicht nur bestens betreut wenn es um die Behandlung und Prophylaxe von Verletzungen geht.
Neben den Räumlichkeiten zum Stabi- und Krafttraining finden wir bei Refit mit Kryo-Sauna und Co. auch
Top-Möglichkeiten zur Verbesserung der Regeneration vor. 
