---
title: "Würdigung Team IngWE"
date: 2023-10-28T12:34:21+02:00
draft: false
toc: false
images:
tags:
  - team
---

![Team OB](/img/2023-10-27-Team-OB.jpg)

Das **TEAM WEIMARER INGENIEURE** erhielt am gestrigen Freitag für seine sportlichen Erfolge in der Triathlon-Bundesliga
Saison 2023 eine Würdigung der Stadt Weimar. Dazu lud Oberbürgermeister Peter Kleine die Mannschaft in den großen Saal
des Weimarer Rathauses ein.

In seiner Rede bedankte sich Peter Kleine für den herausragenden Einsatz der Athleten sowie der Mannschaftsleitung und
überreichte dem Vereinsvorstand gleich zwei Fördermittelbescheide zur Förderung sowohl der 1. als auch der 2.
Bundesliga-Mannschaft.
Auch die sehr gute Arbeit im Nachwuchsbereich der Triathlon-Abteilung des HSV Weimar e.V. kam dabei zur Sprache.

Tom Eismann nutzte die Gelegenheit sich im Namen des gesamten Teams bei allen Unterstützern, Förderern und Sponsoren
zu bedanken, die zusammen mit Vertretern des Stadtsportbundes und des Landessportbundes an der Veranstaltung teilnahmen.
Als besonders emotionalen Moment werden wir die Überreichung eines Präsents an unseren langjährigen Partner Dr.
Reinhard Böber vom Hauptsponsor Glatt in Erinnerung behalten. Mit Glatt als Sponsor der ersten Stunde, begleitete
Dr. Böber den Aufbau und die Entwicklung der Mannschaft seit dem Jahr 2008.

Alle Anwesenden blickten voller Vorfreude auf die Neuauflage des Weimarer Kulturstadt-Triathlons, der am 16.06.2024
unter anderem Austragungsort für die 2. Triathlon-Bundesliga Nord sein wird. Insbesondere die Sportler können dieses
Heimrennen kaum erwarten, um sich direkt vor Weimars Kulisse zu präsentieren. \
Für die Sportstadt Weimar bietet das Event erneut die Möglichkeit sich von seiner besten Seite zu zeigen.
Dementsprechend sagte Oberbürgermeister Kleine die Unterstützung der Stadt für dieses Sportprojekt zu.
