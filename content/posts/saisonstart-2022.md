---
title: "Saisonstart 2022"
date: 2022-05-27T08:30:37+02:00
draft: false
toc: false
author: Johann Reinhardt
images:
tags:
  - 1. Bundesliga
  - 2. Bundesliga Nord
  - Rennen
---

#### Trotz Ausfall-Pech: Ingenieure wollen im Kraichgau bestehen

<p><span class="inactive"><small>von Johan Reinhardt</small></span></p>

Am Samstag gehen die Triathleten des HSV Weimar – **TEAM WEIMARER INGENIEURE** in ihre 13. Saison
in der Triathlon-Bundesliga – nach 2019 das zweite Mal im Oberhaus. Das Ziel unter den mit
internationalen Spitzensportlern und Olympiastartern gespickten 16 Teams ist klar: Die Thüringer
wollen anders als noch vor drei Jahren den Klassenerhalt schaffen. Zum Auftakt im Kraichgau über
die Sprintdistanz von 750 Meter Schwimmen, 20 Kilometer Radfahren und 5 Kilometer Laufen müssen die
Sportliche Leitung um Folker Schwesinger und Tom Eismann allerdings mehr improvisieren, als ihnen
liebt ist.  
Felix Günther und der Lüneburger Neuzugang Timo Behrens haben mit den Nachfolgen einer Corona-Infektion
zu kämpfen, der bundesligaerprobte Sachse John Heiland fehlt aus privaten Gründen und der Erfurter
Leistungsträger Marcel Lehmberg startet in Ägypten erstmals im internationalen Europacup für Deutschland.
Neben den arrivierten Kräften um den frischgebackenen Deutschen Hochschulmeister Alexander Kull
(Rudolstadt), Theo Sonnenberg (Jena) und Ricardo Ammarell (Weimar) feiern deshalb die beiden 17-jährigen
Youngsters Leon Fischer (Weimar) und Aaron Hüter (Jena) ihre Bundesliga-Debüts.  
„Zu der Aufstellung haben uns sicher ein Stückt weit die Umstände gezwungen. Auf der anderen Seite haben
Aaron und Leon gerade im Schwimmen und Laufen enorme Fortschritte gemacht, sodass ich wenig Bedenken habe.
Beide haben die Möglichkeit, auch in der ersten Liga gut zu bestehen. Gerade das Schwimmen ist in der Liga
ein Schlüssel zum Erfolg“, zeigt sich Eismann trotzdem optimistisch und ergänzt mit Blick auf das Saisonziel des
Klassenerhalts: „Wenn wir mit voller Mannschaftsstärke antreten, erhoffe ich mir durchaus einen
einstelligen Platz. Mit dem umgebauten Team, ist es nun schwer zu sagen. Das Ziel für Kraichgau sollte
sein, dass die fünf Jungs an ihr Leistungsmaximum herankommen.“  
Nicht nur für die erste Mannschaft der Ingenieure startet am Wochenende die Bundesligasaison, auch das
zweite Team, in der eigentlich die jungen Athleten langsam herangeführt werden sollen, möchte am Sonntag
in Gütersloh in der 2. Bundesliga Nord bestehen.  
„Neben unseren Routiniers um Henry Beck und Peter Lehmann werden aufgrund der personellen
Ausfälle wohl Theo Sonnenberg und Ricardo Ammarell aus dem Kraichgau nachreisen und ein zweites Mal
starten“, so Eismann. 

