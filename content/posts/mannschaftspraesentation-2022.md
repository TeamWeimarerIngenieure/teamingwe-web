---
title: "Mannschaftspräsentation 2022"
date: 2022-05-07T19:33:27+02:00
draft: false
toc: false
images:
tags:
  - sponsoring
  - 1. Bundesliga
  - 2. Bundesliga Nord
---

![Mannschaftsbild](/img/Mannschaftsbild-2022.jpg "Foto: Peter Hansen")

Beim Hauptsponsor [Glatt Ingenieurtechnik GmbH](https://www.glatt.com) stellte das
**TEAM WEIMARER INGENIEURE** gestern seinen Athletenkader und seine Ziele für die
bald beginnende Triathlon-Saison 2022 vor.
Mit 17 Sportlern geht die Mannschaft ihr ambitioniertes Projekt an, den Klassenerhalt sowohl
in der 1. Bitburger 0.0% Triathlon-Bundesliga als auch in der 2. Bundesliga Nord zu schaffen. Dabei wird vor
allem auf bekannte Gesichter und die Leistungsträger aus den vergangenen Jahren gesetzt. Mit
Timo Behrens (RSC Lüneburg) und Marcus Herbst (Triathlonfüchse Osterburg) haben zwei Neuzugänge
ihr Zweitstartrecht für das **TEAM WEIMARER INGENIEURE** gelöst und stellen sich für die
anstehende Saison somit in den Dienst des Teams.  
Sowohl Leon Fischer (HSV Weimar) als auch Aaron Hüter (Triathlon Jena), die in den beiden
vergangenen Jahren aus dem Nachwuchsbereich in die Mannschaft gewachsen sind, werden die
zunehmenden Einsätze in der 2. Bundesliga nutzen, um wichtige Erfahrung zu sammeln. Sie werden
von den Routiniers Ricardo Ammarell, Henry Beck, Tom Gorges (alle HSV Weimar) sowie Peter
Lehmann (SV Elbland) und Theodor Popp (TSV 1880 Gera-Zwötzen) unterstützt. Mit Tilman Kühne
(Triathlon Nordhausen), Philipp Leiteritz (Triathlon Jena) und Pascal Unbehaun (HSV Weimar)
werden gleich drei Athleten ihr Bundesliga-Debüt feiern.  
Zu den Vielstartern im Weimarer Erstligaaufgebot werden Alexander Kull (HSV Weimar), Marcel
Lehmberg (LTV Erfurt), Theo Sonnenberg (Triathlon Jena) sowie John Heiland (SV Elbland) und
Felix Günther (HSV Weimar) zählen.  

Die folgende Übersicht fasst alle Athleten der Saison 2022 zusammen:

* Ricardo Ammarell <span class="inactive"><small>HSV Weimar</small></span>
* Henry Beck <span class="inactive"><small>HSV Weimar</small></span>
* Timo Behrens <span class="inactive"><small>RSC Lüneburg</small></span>
* Leon Fischer <span class="inactive"><small>HSV Weimar</small></span>
* Tom Gorges <span class="inactive"><small>HSV Weimar</small></span>
* Felix Günther <span class="inactive"><small>HSV Weimar</small></span>
* John Heiland <span class="inactive"><small>SV Elbland</small></span>
* Marcus Herbst <span class="inactive"><small>Triathlonfüchse Osterburg</small></span>
* Aaron Hüter <span class="inactive"><small>Triathlon Jena</small></span>
* Tilman Kühne <span class="inactive"><small>Triathlon Nordhausen</small></span>
* Alexander Kull <span class="inactive"><small>HSV Weimar</small></span>
* Peter Lehmann <span class="inactive"><small>SV Elbland</small></span>
* Marcel Lehmberg <span class="inactive"><small>LTV Erfurt</small></span>
* Philipp Leiteritz <span class="inactive"><small>Triathlon Jena</small></span>
* Theodor Popp <span class="inactive"><small>TSV 1880 Gera-Zwötzen</small></span>
* Theo Sonnenberg <span class="inactive"><small>Triathlon Jena</small></span>
* Pascal Unbehaun <span class="inactive"><small>HSV Weimar</small></span>

