---
title: "Erfolg bei Regionalliga in Görlitz"
date: 2021-07-11T14:06:36+02:00
draft: false
toc: false
images:
tags:
  - regionalliga
  - rennen
---


![Team Rad](/img/2021-07-10-Team-Rad-Goerlitz.jpg)

Am gestrigen Sonnabend fand in Görlitz der erste nicht dezentral ausgetragene Regionalliga-Wettkampf der Saison 2021 statt. Gestartet wurde in einem spannenden Format, bei dem zunächst ein 40km Teamzeitfahren absolviert wurde. Anschließend erfolgte ein Swim & Run im Jagdstartmodus mit den Zeitabständen des Zeitfahrens.

Bei diesem Teamwettbewerb bewiesen Aljoscha Willgosch, Henry Beck, Theodor Popp und Pascal Unbehaun eine ausgeglichene Leistung auf sehr hohem Niveau. Nach dem Mannschaftszeitfahren konnte die Mannschaft mit einem Vorsprung von 23 Sekunden den zweiten Teil des Wettkampfes in Angriff nehmen. Insbesondere auf der Laufstrecke konnte der Vorsprung auf knapp 40 Sekunden weiter ausgebaut werden. Somit siegte im dritten Wettbewerb der Regionalliga-Saison das **TEAM WEIMARER INGENIEURE** erneut und baute seine Tabellenführung nach drei von fünf Wettkämpfen weiter aus.
