---
title: "Mannschaft des Jahres 2021"
date: 2022-05-22T14:28:07+02:00
draft: false
toc: false
images:
tags:
  - aufstieg
  - 1. bundesliga
  - 2. bundesliga nord
  - auszeichnungen
---

![Mannschaft Sportlergala](/img/2022-05-20-Sportlergala-Mannschaft.jpg)

Am vergangenen Freitag fand die Sportlergala der Stadt Weimar statt, bei der die besten Sportlerinnen,
Sportler, Nachwuchsathletinnen und Athleten sowie Sportfunktionäre des Sportjahres 2021 geehrt wurden.
Das **TEAM WEIMARER INGENIEURE** wurde nach den Aufstiegen in die 1. Bitburger 0.0% Triathlon-Bundesliga und in die
2. Bundesliga Nord mit der Ehrung zur Sportmannschaft des Jahres ausgezeichnet.
