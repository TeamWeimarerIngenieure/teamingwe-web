---
title: "Neuzugang Struan Bennet"
date: 2024-02-19T17:00:00+01:00
draft: false
toc: false
images:
tags:
  - team
---

<img style="margin: auto" src="/img/2024-02-Struan-Bennet.png" width="400px" alt="Struan Bennet" />

In der Saison 2024 heißt das **TEAM WEIMARER INGENIEURE** Neuzugang Struan Bennet in seinen Reihen willkommen.  
Der 19-jährige Schotte, der in seiner Heimat dem University of Stirling Triathlon Club angehört, startete im
vergangenen Jahr mehrfach beim Europa-Cup der Junioren und belegte bei den britischen Meisterschaften der
Junioren im Sprint-Triathlon den 12. Platz.  
In der kommenden Saison möchte Struan auch in der 1. Bitburger 0.0% Triathlon-Bundesliga für Furore sorgen
und mit seinem Ehrgeiz das **TEAM WEIMARER INGENIEURE** bereichern.
