---
title: "ingWE-Ultras zum DTU-Triathlon-Held nominiert"
date: 2022-08-09T16:28:29+02:00
draft: false
toc: false
images:
tags:
  - 1. Bundesliga
  - 2. Bundesliga Nord
  - Rennen
  - Support
  - IngWeUltras
---

![Flagge](/img/2022-08-09-Abstimmung-DTU-Held.jpg)

#### Supporter des TEAM WEIMARER INGENIEURE stehen zur Wahl zum DTU-Triathlon-Held\*in des Monats Juli

Wie einige aufmerksame Beobachter des Vereins vielleicht festgestellt haben, wird unser Bundesliga-Team
seit geraumer Zeit von den sogenannten „ingWE-Ultras“ unterstüzt. Die Fan-Gruppierung macht sich nicht
nur durch lautstarke Anfeuerungen, sondern auch mit sichtbaren Aktionen an der Wettkampfstrecke bemerkbar.
Nun wurden sie im Rahmen der DTU-Aktion „Triathlon Held\*in des Monats“ nominiert. In einer siebentägigen
Abstimmung wird nun der Gewinner der Aktion für den Monat Juli bestimmt. Wer die „ingWE-Ultras“ dabei
unterstützen möchte, kann bis **16.08.2022** unter folgendem Link seine Stimme abgeben:

[Zur Abstimmung](https://www.triathlondeutschland.de/aktive/triathlon-heldin-des-monats)

