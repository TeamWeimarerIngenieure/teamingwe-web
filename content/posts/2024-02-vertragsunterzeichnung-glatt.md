---
title: "Glatt mit erhöhtem Engagement"
date: 2024-02-26T18:30:00+01:00
draft: false
toc: false
images:
tags:
  - sponsoring
---

![Vertragsunterzeichnung](/img/2024-02-21-Glatt-Technikum.jpg)

Auch in der Saison 2024 hält die [Glatt Ingenieurtechnik GmbH](https://www.glatt.com) dem **TEAM WEIMARER INGENIEURE**
die Treue.  
In der vergangenen Woche trafen sich Athleten und Funktionäre des Teams sowie Vereinsvertreter mit
Unternehmensvertretern zur Unterzeichnung der Sponsoringvereinbarung für die neue Triathlon-Saison. Als langjähriger
Partner, der das Team seit seiner Gründung im Jahr 2008 begleitet, nimmt die
[Glatt Ingenieurtechnik GmbH](https://www.glatt.com) erneut die Rolle des Hauptsponsors ein. Besonders erfreulich ist
die Tatsache, dass Glatt sein Engagement im **TEAM WEIMARER INGENIEURE** nochmals erhöht hat.
<a class="unstyled" href="https://www.glatt.com/">
<img style="margin: auto" src="/img/Glatt-space-flat.jpg" alt="Glatt" />
</a>
Neben dem formellen Akt der Vertragsunterzeichnung durfte die Mannschaft einer Führung durch das Technikum der Firma
beiwohnen. Somit erhielten die Anwesenden einen Einblick in die interdisziplinären Betätigungsfelder der
[Glatt Ingenieurtechnik GmbH](https://www.glatt.com) sowie in die Unternehmensstruktur.  
Mit besonderem Interesse verfolgten die Sportler die Herangehensweise der Projektteams an ihre Projekte, von der Risikoanalyse über Planung
und Entwurf bis hin zur technischen Realisierung. Insbesondere die langfristige Vorbereitung von Großprojekten ließ
unschwer Parallelen zum Ausdauersport erkennen.  

Auch an dieser Stelle bedankt sich das **TEAM WEIMARER INGENIEURE** für das entgegengebrachte Vertrauen und die
gelungene Veranstaltung!
