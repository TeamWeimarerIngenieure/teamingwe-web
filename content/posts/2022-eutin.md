---
title: "Zwei zweite Plätze in Eutin"
date: 2022-06-20T16:48:34+02:00
draft: false
toc: false
images:
tags:
  - 2. Bundesliga Nord
  - Rennen
---

![Theo Sonnenberg Lauf](/img/2022-06-19-Eutin-Theo-Lauf.jpg)

Mit einem zweiten Platz im Gepäck kehrte das **TEAM WEIMARER INGENIEURE** vom
Rennen der 2. Bundesliga Nord aus der Rosenstadt Eutin zurück. Marcel Lehmberg,
Theo Sonnenberg und Ricardo Ammarell konnten im kalten Eutiner See eine starke
Schwimmform abrufen und stiegen mit der ersten größeren Athletengruppe aus dem
Wasser. Philipp Leiteritz und Felix Günther folgten wenig später und fanden sich
in der zweiten Radgruppe ein. Auf den ersten der fünf Radrunden wurden zunächst
drei Ausreißer gestellt, die das Feld beim Schwimmen mit etwa 20 Sekunden
Abstand angeführt hatten. Im weiteren Verlauf der zweiten Disziplin veränderten
sich die Radgruppen nur geringfügig.  
Nach einem Blitzwechsel stürmten Marcel und Theo auf den Positionen eins und drei
auf die Laufstrecke. Mit einer besonders kämpferischen Leistung beendete Theo
den Wettkampf auf dem Silberrang. Auch die anderen Jungs legten sich für das
Mannschaftsresultat beherzt ins Zeug. Es folgten Marcel, Ricardo, Felix und
Philipp auf den Plätzen 6, 19, 25 und 37.  
  
Alle Ergebnisse sind unter folgenden Links zu finden:
  
- [Einzelwertung](https://my.tollense-timing.de/event/results?eventid=188164&lang=de#8_E14D66)
- [Mannschaftswertung](https://my.tollense-timing.de/event/results?eventid=188164&lang=de#8_1E980A)

