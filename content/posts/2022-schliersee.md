---
title: "Zwei Rennen, ein Team!"
date: 2022-07-18T16:53:01+02:00
draft: false
toc: false
author: "Peter Schwesinger"
images:
tags:
  - 1. Bundesliga
  - Rennen
---

{{< rawhtml >}} 

<video width=100% controls autoplay muted>
    <source src="/videos/2022-07-17-Start-Schliersee.mp4" type="video/mp4">
    Your browser does not support the video tag.  
</video>

{{< /rawhtml >}}

<p><span class="inactive"><small>von Peter Schwesinger</small></span></p>

Während in Grimma gerade die Siegerehrung des zweiten Teams stattfand, kämpften sich
unsere Jungs am Schliersee mit letzten Kräften ins Ziel. Der Alpentriathlon in Bayern
bot dabei eine noch atemberaubendere Kulisse.  
Zum ersten Mal war es unserem Trainer Tom Eismann in dieser Erstligasaison möglich, die
vermeintlichen Top-Five aufzustellen. Ohne kurzfristige Ausfälle und mit einer
ordentlichen Vorbereitung, ging es an die Startlinie des Schliersees.  
Timo Behrens bestätigte beim Schwimmen erneut seine große Stärke und stieg als erster
Weimarer aufs Rad, gefolgt von seinen vier Teamkollegen, die alle einen recht guten Tag
im Wasser erwischten.  
Die bekanntermaßen anspruchsvolle und mit reichlich Höhenmetern versehene Radstrecke
rauf zum Spitzingsattel sorgte dafür, dass sich das Feld schnell auseinander zog und
sich nicht die gewohnt großen Radgruppen bilden konnten. Ein Vorteil, den sich die
guten Radfahrer im Feld, darunter auch Alexander Kull, zunutze machten.  
Der abschließende Lauf am Spitzingsee wurde, durch die reichlichen Höhenmeter, nicht
gerade leichter. Timo bewies sein großes Potenzial und verteidigte seine Position mit
einem soliden Lauf auf Platz 26. Alex, der zunächst den Anstrengungen vom Radfahren
Tribut zollen musste, steigerte sich auf der zweiten Laufrunde und folgte knapp dahinter
auf dem 28. Rang. Theo unterstrich ein sehr konstantes Teamergebnis als 32. Unsere
schnellen Läufer Marcel und John zündeten, wie gewohnt, den Turbo und machten zum Ende
des Rennens noch einige Plätze gut. Als 42. und 47. beendeten sie einen richtig starken
Wettkampf, bei dem es wenig auszusetzen gab. Mit Platz 10 in der Teamwertung stand das
beste Ergebnis in der noch jungen Erstligageschichte unseres Teams. Sau stark!  

![Timo Behrens Lauf](/img/2022-07-17-Schliersee-Timo-Lauf.jpg "Foto: Katja Dießner")

Leider entspannte sich mit diesem hervorragenden Resultat die Situation im Kampf um den
Klassenerhalt nicht, da auch die direkten Konkurrenten sehr gute Ergebnisse erzielten.
Die Lage in der Tabelle bleibt somit äußerst angespannt. Trotz alledem überwiegt bei
uns die Freude über den gezeigten Entwicklungsschritt im gesamten Team. Nun wollen wir
den Schwung in die nächsten Wochen mitnehmen.

Hier die Ergebnisse des Rennens:
* [Einzelwertung](https://www.triathlonbundesliga.de/sites/default/files/documents/2022-07/ErgebnislistenErgebnislisteBuLiEinzel.pdf)
* [Mannschaftswertung](https://www.triathlonbundesliga.de/sites/default/files/documents/2022-07/TEAMWERTUNG03TeamsMnnerPunkte.pdf)

