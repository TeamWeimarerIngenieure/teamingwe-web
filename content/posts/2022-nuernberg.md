---
title: "Mit Handicap auf die Verfolgung"
date: 2022-08-08T19:58:30+02:00
draft: false
toc: false
images:
tags:
  - 1. Bundesliga
  - Rennen
---

![Alexander Kull Rad](/img/2022-08-07-Nürnberg-Alex-Rad.jpg "Foto: Theo Bettin")

Beim vorletzten Wettkampf der 1. Bitburger 0.0% Triathlon-Bundesliga 2022 stand für die Athleten ein
Sonderformat bestehend aus zwei Teilwettbewerben auf dem Programm. In den Prolog über
250m Schwimmen, 6.1km Radfahren und 1.5km Laufen starteten die 64 Athleten einzeln und
jeweils im Abstand von 30 Sekunden.  
  
Alle Weimarer Starter - Alexander Kull, Theo Sonnenberg, Marcel Lehmberg und John
Heiland - konnten beim Schwimmen wichtige Sekunden auf die direkt vor ihnen gestarteten
Konkurrenten aus Köln und Baunatal gutmachen. Trotz der soliden Schwimmleistungen
gestaltete sich der kurze Prolog für das **TEAM WEIMARER INGENIEURE** im weiteren
Verlauf etwas zäh. Im Ziel stand mit den Plätzen 25 (Alexander Kull), 37 (John Heiland),
48 (Theo Sonnenberg) und 52 (Marcel Lehmberg) ein eher durchwachsenes Ergebnis zu buche. 
  
Nach einer etwa einstündigen Pause folgte mit einem Verfolgungsrennen über 500m
Schwimmen, 12.2km Radfahren und 3.5km Laufen der zweite Wettkampfteil. Gestartet wurde
mit den Zeitabständen aus dem Prolog, was zur Folge hatte, dass unsere Jungs bereits
mit Handicap ins Finale starteten. Alexander fand sich nach dem Schwimmen in der zweiten
Radgruppe wieder während sich John, Marcel und Theo etwa 40 Sekunden später in der
dritten Gruppe einfanden. Im Verlauf des Radrennens veränderte sich diese Konstellation
nur geringfügig.  
  
Alexander erwischte keinen optimalen Wechsel und startete den abschließenden Lauf am
Ende seiner Radgruppe. Ähnlich erging es Marcel eine knappe Minute später. Theo hingegen
stürmte als erster seiner Gruppe aus der Wechselzone.  
Auf der Laufstrecke holten die Vier nochmals alles aus sich heraus, um die direkten
Konkurrenten im Tabellenkeller hinter sich zu lassen. Mit Platz 13 in der Teamwertung
errang das **TEAM WEIMARER INGENIEURE** ein solides Tagesergebnis. Leider konnte damit
der Abstand zu den Mannschaften aus Köln und Heidelberg in der Ligatabelle nur geringfügig
verringert werden.

* [Einzelwertung](http://www.triathlonbundesliga.de/sites/default/files/documents/2022-08/Einzel%20M%C3%A4nner.pdf)
* [Mannschaftswertung](http://www.triathlonbundesliga.de/sites/default/files/documents/2022-08/Mannschaft%20M%C3%A4nner.pdf)

