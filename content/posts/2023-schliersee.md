---
title: "TEAM WEIMARER INGENIEURE klettert zu Platz 7"
date: 2023-06-26T16:48:05+02:00
draft: false
toc: false
images:
tags:
  - 1. Bundesliga
  - Rennen
---

![Team Ziel](/img/2023-06-25-Schliersee-Mannschaft.jpg)

Beim zweiten Bundesliga-Rennen der Saison konnte das **TEAM WEIMARER INGENIEURE** einen weiteren Erfolg feiern.
Am Schliersee belegte die Mannschaft um Alexander Kull, Marcel Lehmberg, Theo Sonnenberg, John Heiland und Richard Feuer
vor einer herrlichen Alpenkulisse den 7. Platz. Nach dem 10. Rang beim Ligaauftakt in Kraichgau Ende Mai festigte das
Team somit seinen Platz im Mittelfeld der Ligatabelle. Dank der Kletterqualitäten unserer Jungs hat die Mannschaft nach
zwei von fünf Rennen Platz 8 inne und ist ihrem Ziel des Klassenerhalts einen wichtigen Schritt näher gekommen.  
  
Weiter geht es am 8./9. Juli mit den Wettbewerben in Düsseldorf und Verl. Das in die Finals 2023 Rhein-Ruhr eingebettete
Erstliga-Rennen in Düsseldorf wird gleichzeitig der Austragungswettkampf für die Deutschen Triathlon Meisterschaften der
Elite 2023 sein. Am gleichen Wochenende macht die 2. Bundesliga Nord mit einem Mannschaftssprint halt in Verl.  
  
[Mannschaftsergebnis](http://www.triathlonbundesliga.de/sites/default/files/documents/2023-06/Ergebnisse%20M%C3%A4nner.pdf)  
[Ligatabelle](https://my1.raceresult.com/240724/RRPublish/data/pdf?name=10%20Ergebnislisten%7C00%20Ergebnisliste%201.%20Bundesliga&contest=1&lang=de)  
