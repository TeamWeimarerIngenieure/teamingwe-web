---
title: "Doppelsieg 2021"
date: 2021-09-16T13:55:08+02:00
draft: false
toc: false
images:
tags:
  - aufstieg
  - 1. bundesliga
  - 2. bundesliga nord
  - regionalliga
  - rennen
---

![Doppelsieg](/img/2021-09-16-Doppelsieg.jpg)

Für das **TEAM WEIMARER INGENIEURE** der HSV-Triathleten ging am vergangenen Wochenende die erfolgreichste Saison der Mannschaftsgeschichte zu Ende.
Nach dem Abstieg aus der 1. Bitburger 0.0% Triathlon-Bundesliga im Jahr 2019 und der Liga-Pause im Jahr 2020 startete das Team in der Triathlon-Saison 2021 sowohl in der 2. Bundesliga Nord mit dem Ziel den Wiederaufstieg in die Königsklasse der Deutschen Triathlon Liga zu schaffen. Für die Leistungsträger der Mannschaft ist die Startberechtigung im Liga-Oberhaus enorm wichtig: Neben Rennerfahrung bei hochklassig besetzten Rennen werden die Wettkämpfe der 1. Triathlon-Liga mitunter auch für die Qualifikation für internationale Wettbewerbe herangezogen. Um gleichzeitig dem aufstrebenden Nachwuchs aus dem Verein und der Region einen Einstieg in den Bundesligabetrieb zu ermöglichen, wurde eine zweite Mannschaft in der Regionalliga Ost angemeldet. Auch hier wurde das Ziel verfolgt zu den besten Mannschaften der Regionalliga zu gehören und den Aufstieg in die 2. Bundesliga zu erringen. Mit dieser angestrebten Mannschaftskonstellation wird der immense Leistungsprung zwischen Jugend- und Juniorenaltersklassen und dem Elite-Niveau der 1. Triathlon-Bundesliga durch die zweite Mannschaft überbrückt. Diese dient dabei nicht nur als wichtige Vorbereitungsstation des Nachwuchses sondern auch als Plattform für ambitionierte Sportler, die ihre hohe Trainingsbelastung zeitweilig beispielsweise zugunsten von Ausbildung und Studium reduzieren oder aufgrund von Verletzungen ausgebremst werden.

Mit diesem ambitionierten Ziel erwartete die Mannschaft nach über einem Jahr der Wettkampfpause besonders die ersten Wettkämpfe der Saison mit großer Spannung. Während die Regionalliga bereits im April mit dezentralen Wettkampfformaten startete, fand das erste Bundesliga-Rennen in gewohnter Form Ende Juni in Potsdam statt. Zu beiden Auftaktwettbewerben präsentierte sich das **TEAM WEIMARER INGENIEURE** in Bestform, gewann jeweils den Tagessieg und legte somit den Grundstein für eine erfolgreiche Saison. In den weiteren Rennen der 2. Bundesliga Nord agierten die HSV-Triathleten souverän und beendeten die Saison nach drei Wertungsrennen ungeschlagen. Ähnlich erfolgreich erging es den Regionalliga-Delegationen des Teams bei den insgesamt fünf Wettkämpfen der Regionalliga Ost. Mit drei Siegen und zwei zweiten Plätzen wurde die Tabellenführung zu keinem Zeitpunkt aus der Hand gegeben.

In der Gesamteinzelwertung der 2. Bundesliga Nord sind mit Alexander Kull (1. Platz), Marcel Lehmberg (3. Platz) und John Heiland (4. Platz) gleich drei Athleten des Teams in den Top-6 vertreten. In der Regionalliga-Einzelwertung überzeugten Marcel Lehmberg (3. Platz) und und Felix Günther (4. Platz) mit ihren Platzierungen.

Trotz der hervorragenden Ergebnisse sind die Aufstiege beider Mannschaften alles andere als in trockenen Tüchern. Mit der pandemiebedingten Aussetzung der regulären Auf- und Abstiegsregelungen bleibt die Aufstiegsfrage für das **TEAM WEIMARER INGENIEURE** zunächst weiter spannend.
