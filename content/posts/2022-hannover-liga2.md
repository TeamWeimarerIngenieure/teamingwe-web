---
title: "Toller Abschluss"
date: 2022-09-06T19:05:06+02:00
draft: false
toc: false
images:
tags:
  - 2. Bundesliga Nord
  - Rennen
---

![Team Ziel](/img/2022-09-04-Team-Ziel.jpg "Foto: Pia Nowak")

<p><span class="inactive"><small>von Peter Schwesinger</small></span></p>

Im zweiten Rennen des Wochenendes verteidigten die Sportler unserer zweiten
Mannschaft einen hervorragenden dritten Platz in der Gesamtwertung der 2.
Bundesliga Nord.  
Die Sieben wurde zur Zahl des Tages aus Thüringer Sicht. Das Damen-Triathlonteam
Thüringen und das Herrenteam des LTV Erfurt, startend in der Südstaffel, welche
uns am Vorabend noch so lautstark zum Klassenerhalt gepusht hatten, belegten wie
unser Team den siebten Platz in der Tageswertung. Eine starke Leistung und an
dieser Stelle nochmals vielen Dank für eure bedingungslose Unterstützung! 

An der Startlinie begannen Leon und Felix als erstes Paar ihren Wettkampf.
Die gute Arbeit im Wasser setzte sich aus Weimarer Sicht fort, fanden sich die
beiden Youngster doch prompt in der ersten Radgruppe wieder. Dort zeigte Felix
seine gute Form, in dem er das Feld, an den zahlreichen Zuschauern vorbeifahrend,
zeitweise anführte. Gemeinsam mit Leon legte er darauf auch einen schnellen Lauf
hin. Die Teamtaktik ging an dieser Stelle, auch dank der hervorragenden Vorbereitung
auf das Rennen und der Abstimmung untereinander, auf.  

Mit der Staffelübergabe legten mit Henry und Pascal zwei richtige Maschinen als
zweites Paar los. Vermeintliche Nachteile für Pascal beim Schwimmen, sollten mit
einem herausragenden Einsatz beim Radfahren durch Henry wett gemacht werden. Im
Alleingang setzte unsere Oldgun dies großartig in die Tat um und fuhr die
Verfolgergruppe, mit Pascal im Windschatten, bis knapp an die Spitze heran.  
Pascal stieg somit verhältnismäßig erholt vom Rad und konnte beim abschließenden Lauf
noch einmal alles aus sich rausholen, während Henry sein Rennen ohne Druck beenden
konnte. Für einen Sprung an die Spitze reichte es schlussendlich nicht mehr, trotzdem
konnten alle Beteiligten mit diesem Rennen zufrieden sein.  
Platz 3 in der Abschlusstabelle der 2. Bundesliga Nord und der 14. Rang in der 1.
Bundesliga bedeuteten einen traumhaften Saisonabschluss. Unser Chefcoach Tom Eismann
konnte zusammen mit Co-Teamleiter Aljoscha Willjosch, der die zweite Mannschaft
betreute, mehr als zufrieden sein.

[Mannschaftsergebnis](http://www.triathlonbundesliga.de/sites/default/files/documents/2022-09/2.%20Liga%20Nord%20M%C3%A4nner.pdf)  
[Ligatabelle](https://www.triathlonbundesliga.de/sites/default/files/documents/2022-09/Tabelle%20Norden%20M%C3%A4nner%202022.pdf)  
