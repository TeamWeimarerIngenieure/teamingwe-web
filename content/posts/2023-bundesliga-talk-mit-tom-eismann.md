---
title: "Bundesliga-Talk mit Tom Eismann"
date: 2023-07-20T15:48:56+02:00
draft: false
toc: false
images:
tags:
  - 1. Triathlon Bundesliga
  - Bundesliga-Talk
---

![Tom Eismann](/img/2023-06-25-Schliersee-Tom.jpg "Foto: Tom Gorges")

Im ersten Teil des aktuellen [Bundesliga-Talk](https://www.triathlonbundesliga.de/aktuelles/07-2023/tom-eismann-anne-neidhardt-und-sabrina-fleig-im-bundesliga-talk)
ist **TEAM WEIMARER INGENIEURE** Trainer Tom Eismann zu Gast.

{{< rawhtml >}} 

<audio controls preload="auto">
 <source src="https://www.triathlonbundesliga.de/sites/default/files/2023-07/T%C3%BCbingen.wav">
</audio>

{{< /rawhtml >}}
