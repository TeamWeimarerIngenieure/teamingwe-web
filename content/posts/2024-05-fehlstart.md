---
title: "Fehlstart in die Saison"
date: 2024-05-26T19:43:21+02:00
draft: false
toc: false
images:
tags:
  - Rennen
  - 1. Bundesliga
  - 2. Bundesliga Nord
---

<figure style="margin: auto">
    <img src="/img/2024-05-25-Kraichgau-Mack.jpg" alt="Philipp Mack">
    <figcaption style="text-align: right">Foto: Tom Gorges</figcaption>
</figure>

Der gestrige Saisonstart des **TEAM WEIMARER INGENIEURE** in sowohl die 1. als auch in die 2. Triathlon
Bundesliga ist gehörig missglückt. Nach dem verletzungs- und krankheitsbedingten Ausfällen von Alexander
Kull und Struan Bennet landete das Quartett Philipp Mack (43.), Phil Pfeifer (46.), Leon Fischer (51.)
und Theo Sonnenberg (55.) im Kraichgau auf einem unbefriedigenden 15. Rang.  

Auch in Freilingen verfehlte das **TEAM WEIMARER INGENIEURE** in der Besetzung Stig Rudolph (25.), Philipp
Leiteritz (32.), Hannes Ruhnke (40.), Henry Beck (55.) und Julius Domnick (63.) mit Platz 11 die eigene
Zielstellung sich unter den besten zehn Mannschaften zu platzieren.

[Einzelergebnis 1. Triathlon Bundesliga](https://www.triathlonbundesliga.de/sites/default/files/documents/2024-05/Kraichgau%20M%C3%A4nner%20Einzel.pdf)

[Mannschaftsergebnis 1. Triathlon Bundesliga](https://www.triathlonbundesliga.de/sites/default/files/documents/2024-05/Kraichgau%20M%C3%A4nner%20Team.pdf)

[Ergebnisse 2. Triathlon Bundesliga Nord](https://www.ingweultras.com/scoreboard/) (noch nicht verfügbar)
