---
title: "Vertragsunterzeichnung"
date: 2022-02-22T18:30:47+02:00
draft: false
toc: false
images:
  - /img/Glatt-space.jpg
tags:
  - sponsoring
---

![Vertragsunterzeichnung](/img/2022-02-22-Vertragsunterzeichnung.jpg)

Am 22. Februar 2022 wurde der Sponsorenvertrag mit dem Hauptsponsor des **TEAM WEIMARER INGENIEURE**,
der [Glatt Ingenieurtechnik GmbH](https://www.glatt.com/), unterzeichnet.

[![Glatt](/img/Glatt-space-flat.jpg)](https://www.glatt.com/)

Wir danken der [Glatt Ingenieurtechnik GmbH](https://www.glatt.com/), besonders Herrn Dr. Böber, für
die langjährige Unterstützung.
Besonders erfreulich ist die Zusage der [Glatt Ingenieurtechnik GmbH](https://www.glatt.com/) die
hervorragende Zusammenarbeit in den kommenden Jahren fortzuführen.

