---
title: "Neuzugänge überzeugen in Grimma"
date: 2024-08-19T19:21:20+02:00
draft: false
toc: false
images:
tags:
  - Rennen
  - 2. Bundesliga Nord
---

![Leon](/img/2024-08-18-Grimma-Leon.jpg)

Mit einem Durchschnittsalter von gerade einmal 16,8 Jahren schickte das **TEAM WEIMARER INGENIEURE** eines
der jüngsten Teams der Zweitligageschichte zum Saisonfinale nach Grimma.  
Nachdem die Mannschaft beim vorangegangenen Saisonrennen in [Salzgitter](/posts/2024/08/team-ingwe-weiter-im-aufwind/)
ihren Platz unter den besten fünf Teams der 2. Triathlon Bundesliga Nord gefestigt hatte, nutzte die
Mannschaftsleitung das Ligafinale, um weiteren Neuzugängen aus dem Nachwuchsbereich erste Starts in
der Bundesliga zu bescheren.  

![Thorben](/img/2024-08-18-Grimma-Thorben.jpg)

Herausragend präsentierte sich dabei Phil Pfeifer als dritter der Tageseinzelwertung. Leon Fischer (20.)
führte nach einem lehrbuchreifen, zweiten Wechsel zeitweilig das Rennen an, hatte jedoch Probleme seinen
Laufrhythmus zu finden und musste die Spitze ziehen lassen.  
[Juniorteammitglied](http://www.hsv-weimar-triathlon.de/mannschaften/juniorteam/) Thorben Hädrich (37.)
überzeugte mit soliden Leistungen in allen Disziplinen. Stig Rudolph (56.) und Paul Adelt (70.) hatten bereits
zu Beginn des Rennens mit Widrigkeiten zu kämpfen. So machte Paul beispielsweise direkt nach dem Start
Bekanntschaft mit der berühmt-berüchtigten "Waschmaschine" im Männerfeld.  
In diesem lehrreichen Wettkampf belegte das junge Quintett in der Tageswertung der Teams einen soliden
siebten Platz und besiegelte damit Rang fünf in der Gesamtwertung der 2. Triathlon Bundesliga Nord 2024.  
  
[Einzelergebnis](https://www.triathlonbundesliga.de/sites/default/files/documents/2024-08/Ergebnisse%202.%20Liga%20Nord%20Grimma%20Einzel%20M%C3%A4nner.pdf)  
[Mannschaftsergebnis](https://www.triathlonbundesliga.de/sites/default/files/documents/2024-08/Ergebnisse%202.%20Liga%20Nord%20Grimma%20Team%20M%C3%A4nner.pdf)  
[Ligatabelle](https://my1.raceresult.com/292060/RRPublish/data/pdf?name=10%20Ergebnislisten%7C00%20Ergebnisliste%202.%20Bundesliga%20Nord&contest=5&lang=de)  
[Scoreboard](https://www.ingweultras.com/scoreboard/)  
