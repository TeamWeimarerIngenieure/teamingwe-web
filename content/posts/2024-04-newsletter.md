---
title: "Newsletter der ingWE-Ultras"
date: 2024-04-20T12:52:42+02:00
draft: false
toc: false
images:
tags:
  - team
  - ultras
---

![ingWE-Ultras Newsletter](/img/ultras_newsletter.jpg)

Die Triathlonbundesliga-Saison 2024 wirft bereits große Schatten voraus
und die ersten wichtigen Termine rund um das **TEAM WEIMARER INGENIEURE**
stehen kurz bevor. Wir freuen uns riesig, dass unser Team, mit dem
Einzug der Sommermonate, nun bald wieder auf der großen Triathlon-Bühne
zu sehen sein wird. Viel hat sich seit dem vergangenen Herbst getan und
wir können es kaum erwarten, unsere Athleten für die diesjährigen
Rennen vorzustellen. Neben altbekannten Leistungsträgern, werden wir
zu unserer Teampräsentation, die am 23.04.2024 um 16:00 Uhr im
Gewölbekeller der Stadtbibliothek Weimar stattfindet, auch zahlreiche
neue Gesichter vorstellen können.

Bereits jetzt können wir voller Vorfreude verkünden, dass es ab dieser
Saison, in Kooperation mit unserer Fan-Gemeinschaft, den
[ingWE-Ultras](https://ingweultras.com),
einen exklusiven [Newsletter](/newsletter/) geben wird. In ihrem eigens gestalteten
Blog, werden unsere engsten Supporter über sämtliche Themen rund um das
Team, sowie vordergründig von Rennwochenenden aus ihrer Sicht berichten
und die mit zahlreichen Fotos unterlegten Erlebnisse mit allen
Interessierten teilen.

Der Newsletter wird etwa einmal im Monat und in der Regel nach den
Rennwochenenden versendet.
