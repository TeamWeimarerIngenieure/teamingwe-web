---
title: "Aufstieg in die 1. Bitburger 0.0% Triathlon-Bundesliga"
date: 2021-12-05T13:44:23+02:00
draft: false
toc: false
images:
tags:
  - aufstieg
  - 1. bundesliga
  - 2. bundesliga nord
  - rennen
  - regionalliga
---

![Aufstieg](/img/2021-12-05-Aufstieg.jpg)

Der [Ausscheidungswettkampf](https://www.thueringer-allgemeine.de/sport/weimarer-triathleten-bejubeln-bundesliga-aufstieg-id234017993.html) um den Aufstieg in die 1. Bitburger 0.0% Triathlon-Bundesliga gegen den
SV Würzburg (am 04. Dezember 2021 in Würzburg) wurde durch das **TEAM WEIMARER INGENIEURE**
überzeugend gewonnen. Damit wird für den [HSV Weimar e.V.](https://www.hsv-weimar.de/) im
Jahr 2022 sowohl eine Mannschaft in der 1. Bitburger 0.0% Triathlon-Bundesliga als auch in der 2. Triathlon
Bundesliga Nord starten.

Der Ausscheidungswettkampf wurde als Swim & Run ausgetragen. Das Schwimmen fand in der Würzburger
Schwimmhalle (50 Meter Bahn) und das Laufen im Stadion der Universität statt. Leider waren die
Wetterbedingungen denkbar schlecht – 2 bis 3 ⁰C Außentemperatur, schneebedeckte Laufbahn (wurde
vor dem Wettkampf geräumt) und leichter Regen.

Nach dem Schwimmen lag die Mannschaft aus Würzburg in Front. Unsere Jungs mussten also im Laufen
alles geben, um diesen Rückstand aufzuholen. Mit einer ganz starken Laufleistung konnte der Wettkampf
am Ende überzeugend gewonnen werden.

Wir bedanken uns bei allen Unterstützern für ihr Engagement, dem Weimarer Oberbürgermeister,
Herrn Kleine, für seine übermittelten Glückwünsche.

