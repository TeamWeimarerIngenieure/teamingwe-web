---
title: "Die Finals 2022"
date: 2022-06-27T15:07:53+02:00
draft: false
toc: false
images:
author: "Peter Schwesinger"
tags:
  - 1. Bundesliga
  - Deutsche Meisterschaft
  - Rennen
  - Die Finals
---

![Theo Sonnenberg Lauf](/img/2022-06-26-Berlin-Theo-Lauf.jpg "Foto: Tom Gorges")

<p><span class="inactive"><small>von Peter Schwesinger</small></span></p>

Ein Wochenende mit hohem Aufwand liegt hinter uns und das mussten wir erstmal sacken lassen.
Das Wichtigste vorweg: Wieder Platz 14 im zweiten Rennen der 1. Bundesliga, wieder zwei
Mannschaften hinter uns gelassen, aber nicht dieselben! Das ist der Knackpunkt in unserer
Gefühlswelt, da wir, nach erneut großem Einsatz, auf Platz 15 und damit auf einen Abstiegsplatz
in der Tabelle gerutscht sind.  
  
Die Erkenntnis des Wochenendes lautet und das kann man mit den Erfahrungen aus dem Kraichgau
ganz gut unterstreichen: Die Leistungsdichte ist so hoch, dass man wirklich volle 100% Leistung
und eine Top-Vorbereitung braucht, um weiter oben anklopfen zu können.  
Der Lichtblick: Es gibt noch drei Möglichkeiten zu beweisen, dass wir nicht nur das Potenzial
dazu haben! Groß war der Abstand nicht. Lediglich sechs Platzziffern Rückstand auf Platz 11
standen am Sonntag zu Buche.  
Theo Sonnenberg überzeugte erneut mit Platz 39 und war somit
Stärkster im Team, knapp vor Alexander Kull (41.). Unser Debütant Timo Behrens (51.) zeigte
seine große Stärke beim Schwimmen, musste erst beim Laufen dem krankheitsbedingten
Trainingsrückstand Tribut zollen. Der Rennverlauf für John Heiland (64.) sah genau umgekehrt
aus. Nachdem ihm der Sprung in eine der vorderen Radgruppen verwehrt blieb, sammelte er beim
Laufen noch einige Konkurrenten ein. Leon sicherte das Mannschaftsergebnis mit Platz 72 ab.
Trotz Abiturprüfungen in den letzten Wochen stand er ein zweites Mal mit unbändigen Einsatz zur
Stelle, als sich eine Lücke im Team auftat.  

Unter dem Strich ist allen Beteiligten klar, um was es in den nächsten Wochen geht und worauf
es ankommt, dass wir die größer werdende Herausforderung Klassenerhalt in dieser Saison meistern.

Die Ergebnisse der Finals in Berlin sind unter folgenden Links zu finden:

* [Einzelwertung](https://www.triathlonbundesliga.de/sites/default/files/documents/2022-06/ErgebnislistenErgebnislisteBULI_0.pdf)
* [Mannschaftswertung](https://www.triathlonbundesliga.de/sites/default/files/documents/2022-06/TEAMWERTUNG03TeamsMnnerPunkte.pdf)

