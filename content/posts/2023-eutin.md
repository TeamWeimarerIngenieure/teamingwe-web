---
title: "Sieg zum Zweitliga-Auftakt"
date: 2023-06-19T21:18:56+02:00
draft: false
toc: false
images:
tags:
  - 2. Bundesliga Nord
  - Rennen
---

![Team Ziel](/img/2023-06-18-Eutin-Siegerehrung.jpg)

<p><span class="inactive"><small>von Tom Eismann</small></span></p>

Zum Saisonauftakt der 2. Bundesliga Nord präsentierte sich das **TEAM WEIMARER INGENIEURE** in bester Verfassung.
Mit Leon Fischer, Aaron Hüter, Richard Feuer, Felix Günther und Peter Lehmann konnte die Teamleitung eine
hervorragende  Besetzung aufbieten. Die fünf Sportler waren motiviert, dem Auftaktergebnis der
Erstliga-Mannschaft in nichts nachzustehen und starteten mit breiter Brust in den Wettkampf.
Schon im Schwimmen konnten sich die Sportler im Vorderfeld festsetzen. Besonders Leon Fischer setzte sich hier
in Szene und verließ als Erster der Weimarer Sportler knapp hinter dem Führenden das Wasser. Auf dem engen,
schnellen Radkurs fand sich die komplette Weimarer Vertretung bald in der ersten Gruppe wieder. Taktisch
versiert stiegen alle Sportler des Teams ganz vorn vom Rad und schafften sich so eine hervorragende
Ausgangsposition für den abschließenden Lauf. Diesen bewältigte Richard Feuer am schnellsten und konnte sich
im Einzelergebnis einen großartigen fünften Platz sichern. Peter Lehmann erreichte einen hervorragenden Platz
11 und unterstrich einmal mehr, dass ich die Teamleitung blind auf den Routinier verlassen kann. Felix Günther,
der nach der zweiten Radzeit des Tages auch über die sechs Laufrunden flog, wurde abschließend auf Platz 14
geführt. Aaron Hüter erreichte nach großen Kampf einen guten 21. Platz. Leon Fischer komplettierte das
Teamergebnis auf Rang 35.  
  
Mit Platzziffer 51 errang das **TEAM WEIMARER INGENIEURE** einen hervorragenden ersten Platz im Tagesergebnis und
kann nach dem ersten Rennen fröhlich von der Spitze der Tabelle winken.
