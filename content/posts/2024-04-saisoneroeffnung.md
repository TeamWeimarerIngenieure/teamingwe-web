---
title: "Saisoneröffnung"
date: 2024-04-24T12:52:42+02:00
draft: false
toc: false
images:
tags:
  - team
---

![Saisoneröffnung Pressekonferenz](/img/2024-04-23-Seasonopener-9311.jpg)

Am gestrigen Dienstag fand die Saisoneröffnungsveranstaltung des **TEAM WEIMARER INGENIEURE** im Gewölbekeller der
Stadtbibliothek Weimar statt. Zahlreiche Gäste, unter ihnen Peter Kleine, Oberbürgermeister der Stadt Weimar,
Sponsoren und Partner, sowie Vertreter des HSV Weimar und eine Vielzahl neugieriger Nachwuchstriathleten erlebten
die Vorstellung unserer Athleten für die bald startende Bundesligasaison 2024.

Neben der Präsentation der Bundesliga-Termine sowie der sportlichen Ziele für die kommende Saison standen
Abteilungsleiter Detlev Schmolinske, [Kulturstadttriathlon](https://www.kulturstadttriathlon.de)-Organisator
Christoph Democh, Leistungsträger Alexander Kull und die Mannschaftsleitung dem Publikum im Rahmen einer
Pressekonferenz Rede und Antwort. Die Mitglieder der [IngWE-Ultras](https://www.ingweultras.com) nutzten zudem
die Gelegenheit die erste Ausgabe ihres brandneuen [Newsletters](https://www.ing-we.de/newsletter/) zu promoten.

![Nachwuchs](/img/2024-04-23-Seasonopener-9290.jpg)

Als besonderes Highlight verteilten die Leistungsträger der Mannschaft Autogrammkarten an die hellauf begeisterten
Nachwuchssportler. Ein informativer und kurzweiliger Nachmittag fand seinen Ausklang bei zahlreichen Gesprächen in
lockerer Atmosphäre.
