---
title: "TEAM WEIMARER INGENIEURE trotzt Krankheitspech"
date: 2022-05-31T16:40:26+02:00
draft: false
toc: false
images:
tags:
  - 1. Bundesliga
  - 2. Bundesliga Nord
  - Rennen
---

![Kraichgau Schwimmen](/img/2022-05-28-Kraichgau-Schw.jpg "Foto: Tom Gorges")

Zum ersten Rennwochenende der Triathlon-Saison 2022 standen mit dem Rennen in Kraichgau
(1. Bundesliga) und dem Mannschaftssprint in Gütersloh (2. Bundesliga Nord) gleich zwei
Wettbewerbe auf dem Programm.  
Aufgrund zahlreicher Krankheitsfälle in den Vorwochen des Liga-Auftakts hatte sich das
**TEAM WEIMARER INGENIEURE** mit einem stark dezimierten Athletenkader zu begnügen. Mit nur
acht Sportlern musste das Wettkampfwochenende in Angriff genommen werden. Alexander
Kull (31.), Theo Sonnenberg (51.), Aaron Hüter (58.), Ricardo Ammarell (63.) und Leon
Fischer (64.) erkämpften am Samstagabend im Kraichgau einen soliden 14. Platz. Die beiden
Erstliga-Debütanten Aaron Hüter und Leon Fischer gaben mit beherzten Rennen ihren Einstand
in Deutschlands höchster Triathlonliga und trugen dazu bei, dass sich das **Team 
Weimarer Ingenieure** nach dem ersten von fünf Rennen in der Ligatabelle oberhalb der
Abstiegsränge befindet.  

Währenddessen bereiteten sich Henry Beck, Peter Lehmann und Philipp Leiteritz auf ihr am
Sonntag in Gütersloh stattfindendes Rennen vor. Da der Mannschaftssprint nicht mit drei
Athleten gestartet werden kann, reisten Theo Sonnenberg und Aaron Hüter unterstützt von
Ricardo Ammarell in der Nacht zum Sonntag weiter nach Gütersloh.  
Trotz der kurzen Nacht für die beiden Doppelstarter fand das Weimarer Quintett von Beginn
an einen zügigen Rhythmus und lag bereits nach dem Schwimmen auf Tuchfühlung mit den
schnellsten Mannschaften. Mit einer geschlossenen Mannschaftsleistung lief das **Team
Weimarer Ingenieure** nach einem spannenden Rennen auf Rang drei ins Ziel.

[Ergebnisse 1. Triathlon-Bundesliga](http://www.triathlonbundesliga.de/ergebnisse)

[Ergebnisse 2. Triathlon-Bundesliga Nord](http://www.triathlonbundesliga.de/2-bundesliga/liga-nord)
