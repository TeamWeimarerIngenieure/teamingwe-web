---
title: "IAB offizieller Partner des Teams"
date: 2024-06-07T21:59:37+02:00
draft: false
toc: false
images:
tags:
  - sponsoring
---

<a class="unstyled" href="https://www.iab-weimar.de/">
<img style="margin: auto; width: 500px" src="/img/IAB-Logo-2.jpg" alt="Logo IAB Weimar"/>
</a>

Zukünftig unterstützt [IAB - Institut für angewandte Bauforschung Weimar](https://www.iab-weimar.de)
das **TEAM WEIMARER INGENIEURE** als offizieller Partner!  

Anlässlich der Vertragsunterzeichnung trafen sich Athleten und Funktionäre
des Teams mit Institutsdirektor Robert Fetter sowie den Vertretern des HSV
Weimar und der Abteilung Triathlon in den Räumlichkeiten des Instituts.  

![Vertragsunterzeichnung IAB](/img/2024-06-06-Vertragsunterzeichnung-IAB-2.jpg)

Im Anschluss durften die Athleten und Verantwortlichen eine Laborbesichtigung
im Technikum des Instituts erleben und die Tätigkeitsfelder des neuen Partners
IAB kennenlernen.  

Das **TEAM WEIMARER INGENIEURE** bedankt sich für das entgegengebrachte
Vertrauen und die spannenden Einblicke. Wir freuen uns auf eine erfolgreiche
Partnerschaft!

