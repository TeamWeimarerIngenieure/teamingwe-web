---
title: "Die Finals 2023"
date: 2023-07-08T22:19:50+02:00
draft: false
toc: false
images:
tags:
  - 1. Bundesliga
  - Rennen
---

![Team Ziel](/img/2023-07-08-Duesseldorf-Wechsel.jpg)

<p><span class="inactive"><small>von Peter Schwesinger</small></span></p>

Erneut heißt es Platz 7 für das **TEAM WEIMARER INGENIEURE** in der 1. Bitburger 0.0% Triathlon-Bundesliga!

In einem top besetzten Rennen landet unser Team ein richtig starkes Ergebnis und untermauert
die bisherigen Saisonleistungen. Besonders imponierend zeigt sich dabei die geschlossene
Mannschaftsleistung. 13 Mannschaften können mindestens einen Athleten vor unserem Ersten im
Ziel vermelden und dennoch gelingt dem **TEAM WEIMARER INGENIEURE** Platz sieben! Dafür
sorgen, wie am Schliersee: Alexander Kull (25), Theo Sonnenberg (29), Richard Feuer (32),
Marcel Lehmberg (34) und John Heiland (44)! Ein klares Indiz dafür, dass nicht nur Team drauf
steht, sondern auch TEAM drin steckt.  
Dass dieses Ergebnis an einem so wichtigen Tag und zum Saisonhighlight gelingt, ist ebenso
ein positives Indiz für erfolgreiches Training. Die Mannschaft hat aus dem vergangen Jahr
definitiv gelernt und ist nun endgültig in der 1. Bitburger 0.0% Triathlon-Bundesliga angekommen.  
In der Tabelle rutscht man zwar auf den 9. Platz ab, jedoch sind die Abstände nach oben
kürzer als nach unten. Das ist beruhigend, sollte aber nicht dazu führen, dass nun die
Spannung ein wenig verloren geht. In Anbetracht der kämpferischen Leistung braucht man sich
keine Sorgen zu machen. Die Defizite im Vergleich zur Konkurrenz beim Schwimmen, wurden wieder
einmal auf dem Rad und insbesondere beim Lauf wett gemacht. So lesen sich die Ergebnisse
umso schöner, denn wir haben auch vier Athleten in den Top 30 der DM-Elite in unseren Reihen!
Nicht nur das, auch Leon Fischer und Aaron Hüter liefern großartig und zeigen einmal mehr,
dass sie sich auf einem sehr guten Weg befinden. Respekt für eure Leistungen, wir sind stolz
auf euch!

[Mannschaftsergebnis](https://www.triathlonbundesliga.de/sites/default/files/documents/2023-07/TEAMWERTUNG03TeamsMnnerPunkte.pdf)  
[Ligatabelle](https://my1.raceresult.com/240724/RRPublish/data/pdf?name=10%20Ergebnislisten%7C00%20Ergebnisliste%201.%20Bundesliga&contest=1&lang=de)  

Die TV-Liveübertragung des Wettkampfes ist in der
[ZDF Mediathek](https://www.zdf.de/sport/die-finals/finals-2023-triathlon-100.html#autoplay=true)
verfügbar.

