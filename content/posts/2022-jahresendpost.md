---
title: "Saisonrückblick 2022"
date: 2022-12-22T08:27:24+01:00
draft: false
toc: false
images:
tags:
  - 1. Bundesliga
  - 2. Bundesliga Nord
  - Support
---

{{< rawhtml >}} 

<video width=100% controls autoplay muted>
    <source src="/videos/2022-09-03-Hannover.mov" type="video/mp4">
    Your browser does not support the video tag.  
</video>
<div style="text-align: right">
<small>Video: Tim Stegemann</small>
</div>

{{< /rawhtml >}}

Mitte Dezember fand unser gemeinsamer Saisonabschluss mit Sportlern, Sponsoren
sowie Vereins- und Verbandsvertretern bei unserem langjährigen Hauptsponsor
[Glatt Ingenieurtechnik Weimar GmbH](https://www.glatt.com/unternehmen/technologiezentren/weimar/)
statt.  
Mit einem ausführlichen Saisonrückblick ließ nicht nur die Teamleitung die
vergangene Triathlon-Bundesligasaison Revue passieren. Auch unsere Athleten
berichteten von ihren Erlebnissen und Erfahrungen des aufregenden Triathlonjahrs
2022 und durchlebten für unsere Unterstützer und Förderer den in letzter Sekunde
errungenen Klassenerhalt in der 1. Bitburger 0.0% Triathlon-Bundesliga noch einmal.  
  
An dieser Stelle bedanken wir uns bei allen Mitstreitern und Sponsoren für die
erfolgreiche Zusammenarbeit im letzten Jahr und wünschen einige frohe und
erholsame Feiertage sowie einen guten Start ins neue Jahr!  
Gleichzeitig erwarten wir mit großer Vorfreude die kommende Triathlonsaison, die
wir mit vielen Bekannten bestreiten werden. Aber auch einige neue Sportler,
Unterstützer und Partner heißen wir im **TEAM WEIMARER INGENIEURE** willkommen.
Nähere Informationen dazu wird es zu gegebener Zeit geben.
