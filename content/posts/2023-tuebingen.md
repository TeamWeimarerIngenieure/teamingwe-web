---
title: "Verflixtes Tübingen"
date: 2023-07-24T16:03:37+02:00
draft: false
toc: false
images:
tags:
  - 1. Triathlon Bundesliga
  - Rennen
---

![Alexander Kull, Marcel Lehmberg Rad](/img/2023-07-23-Tuebingen-Rad.jpg "Foto: Katja Dießner")

<p><span class="inactive"><small>von Peter Schwesinger</small></span></p>

Der Klassenerhalt für das **TEAM WEIMARER INGENIEURE** in der 1. Triathlon-Bundesliga ist praktisch sicher.
Der Abstieg ist nur noch mathematisch möglich, aus sportlicher Sicht jedoch höchst unwahrscheinlich. Das
haben wir einer erneut geschlossenen Mannschaftsleistung zu verdanken.  
Einziger Wermutstropfen war die
Disqualifikation für Alex, der in einer Kurve die Mittellinie überfuhr und anschließend eine Zeitstrafe
kassierte. Diese übersah er auf der hektischen und von Zuschauern gesäumten Laufstrecke in der Tübinger
Innenstadt. Ohne den Besuch in der Penalty Box lief er ins Ziel, was dazu führte, dass sein gutes Ergebnis
in ein DSQ umgewandelt wurde. Ärgerlich!  
Dennoch möchten wir an dieser Stelle den Stellenwert von Alex in
unserem Team hervorheben. In so vielen Rennen hat er dafür gesorgt, dass wir mit guten Ergebnissen nach
Hause gefahren sind und unsere sportlichen Erfolge erreicht haben. Stets war er für das Team da. Diesmal
standen seine Mitstreiter für ihn ein und erkämpften einen noch akzeptablen 13. Platz.  
Theo erwischte einen
sehr guten Tag im Wasser und stieg gemeinsam mit Richard auf das Rad. In der zweiten Radgruppe behaupteten
sie ihre Position bis zum Lauf. Marcel und Alex leisteten die Verfolgungsarbeit in der dritten Radgruppe.
Beim abschließenden Lauf schob sich aus Teamsicht alles zusammen, sodass unser Team erneut sehr kompakt ins
Ziel einlief.  
Mit den Ergebnissen von Theo (24.), Marcel (32.) und Richard (34.) wäre ohne die Disqualifikation vermutlich
Platz 10 drin gewesen. Somit kann man mit der sportlichen Leistung einmal mehr zufrieden sein. In diesem
Sinne gilt es, aus den Fehlern zu lernen, um sich in Zukunft dafür wieder zu belohnen.  
Wir haben wieder einmal einen tollen Tag erlebt und bedanken uns für das spektakuläre Rennen unseres Teams.
Voller Hingabe haben wir die Jungs auf dem gesamten Kurs unterstützt und für Aufsehen am Streckenrand gesorgt!
  
[Mannschaftsergebnis](http://www.triathlonbundesliga.de/sites/default/files/documents/2023-07/TEAMWERTUNG03TeamsMnnerPunkte_0.pdf)  
[Ligatabelle](https://my1.raceresult.com/240724/RRPublish/data/pdf?name=10%20Ergebnislisten%7C00%20Ergebnisliste%201.%20Bundesliga&contest=1&lang=de)
