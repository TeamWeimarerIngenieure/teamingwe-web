---
title: "Partner"
date: 2022-04-05T13:53:39+02:00
draft: false
toc: false
images:
tags:
  - sponsoring
---

Ohne das Engagement und die Unterstützung durch Sponsoren und Partner ist Leistungssport
nicht möglich. Im **TEAM WEIMARER INGENIEURE** haben sich Ingenieurbüros und Unternehmen
aus der Region zusammengeschlossen, um gemeinsam die finanzielle Basis für die Bundesligateams
und damit eine langfristige leistungssportliche Entwicklung des Triathlonsports in Thüringen zu
schaffen.  
Somit bietet das **TEAM WEIMARER INGENIEURE** nicht nur Sportlern eine Bühne. Sowohl
Premiumsponsoren als auch Partner finden sich im Netzwerk des Teams wieder. 
<!-- Der
*Club 100* erlaubt interessierten Unterstützern ein niedrigschwelliges Engagement mit
Spendencharakter. -->  


## OFFIZIELLER HAUPTSPONSOR

<a class="unstyled" href="https://www.glatt.com/">
<img style="margin: auto" src="/img/Glatt-space-flat.jpg" alt="Glatt" />
</a>

## OFFIZIELLER PREMIUMPARTNER

<a class="unstyled" href="https://www.chromecars.de/">
<img style="margin: auto" src="/img/Logo_ChromeCars_transparent.png" alt="ChomeCars" width="500px" />
</a>

## EXKLUSIVPARTNER GESUNDHEIT

<a class="unstyled" href="https://www.refit-jena.de">
<img style="margin: auto" src="/img/refit_Logo_original_0916.png" width="200px" alt="ReFit Therapie und Training" />
</a>

## EXKLUSIVPARTNER MARKETING

<a class="unstyled" href="https://www.adlx.de/">
<img style="margin: auto" src="/img/Logo_ADLX_schwarz-gelb-1.png" width="180px" alt="ADLX" />
</a>

## EXKLUSIVPARTNER BEKLEIDUNG

<a class="unstyled overlay" href="https://www.renerosa.de" target="_blank">
<img style="margin: auto" src="/img/ReneRosa_2024_schwarz.png" width="400px" alt="ReneRosa" />
</a>

## EXKLUSIVPARTNER BIKE-SUPPORT

<a class="unstyled overlay" href="https://rad-doktor.de/" target="_blank">
<img style="margin: auto" src="/img/Logo_RadDoktor_quer.png" width="600px" alt="RadDoktor" />
</a>

## PARTNER

<a class="unstyled" href="https://www.iab-weimar.de/">
<img style="margin: auto" class="overlay" src="/img/IAB-Logo-2.jpg" alt="IAB Weimar" width="300px" />
</a>

## CLUB 100

Ingenieurbüro Dr. Krämer GmbH


##

Möchten Sie zum **TEAM WEIMARER INGENIEURE** gehören? Wir sind auf der Suche nach weiteren
Partnern! [Gerne besprechen wir die Möglichkeiten einer Zusammenarbeit mit Ihnen](/kontakt/).


<!--
## Club 100

Sie interessieren sich für eine Mitgliedschaft im *Club 100*? [Wir freuen uns darauf, von
Ihnen zu hören](/kontakt/)! 
-->


