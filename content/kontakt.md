---
title: "Kontakt"
date: 2022-04-11T20:27:19+02:00
draft: false
toc: false
images:
tags:
  - kontakt
---

## TEAMLEITER {#teamleiter}

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 30%">
            <img class="avatar" src="/img/Folker00657.jpg" alt="Teamleiter" />
        </td>
        <td class="invisible" style="width: 70%">
Folker Schwesinger<br/>
<a href="mailto:folker.schwesinger@ing-we.de">folker.schwesinger@ing-we.de</a>
<a class="unstyled overlay symbol" title="E-Mail-Verschlüsselung" href="http://keyserver.ubuntu.com/pks/lookup?search=0xC7AE525D8C006B3B1D5AB6919A19B422A531589C&fingerprint=on&op=index" target="_blank">🔒</a><a class="unstyled overlay symbol" title="Was ist das?" href="/datenschutz#pgp">🛈</a>
        </td>
    </tr>
</table>

## TEAMMANAGER

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 30%">
            <img class="avatar" src="/img/Ali0005.jpg" alt="Teammanager" />
        </td>
        <td class="invisible" style="width: 70%">
Aljoscha Willgosch<br/>
<a href="mailto:aljoscha.willgosch@ing-we.de">aljoscha.willgosch@ing-we.de</a>
        </td>
    </tr>
</table>

## SPORTLICHER LEITER & TRAINER

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 30%">
            <img class="avatar" src="/img/Tom00713.jpg" alt="Sportlicher Leiter" />
        </td>
        <td class="invisible" style="width: 70%">
Tom Eismann<br/>
<a href="mailto:tom.eismann@ing-we.de">tom.eismann@ing-we.de</a>
        </td>
    </tr>
</table>

## ASSISTENZ DER TEAMLEITUNG

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 30%">
            <img class="avatar" src="/img/765-default-avatar.png" alt="Assistenz Teamleitung" />
        </td>
        <td class="invisible" style="width: 70%">
Ricardo Ammarell<br/>
<a href="mailto:ricardo.ammarell@ing-we.de">ricardo.ammarell@ing-we.de</a>
        </td>
    </tr>
</table>

## POSTANSCHRIFT

HSV Weimar e.V.  
Prager Straße 5  
99427 Weimar
