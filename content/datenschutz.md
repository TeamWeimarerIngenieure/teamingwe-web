---
title: "Datenschutzerklärung"
date: 2022-04-11T20:29:48+02:00
draft: false
toc: false
images:
tags:
---

Verantwortliche Stelle im Sinne der Datenschutzgesetze, insbesondere der
EU-Datenschutzgrundverordnung (DSGVO), ist der HSV Weimar e.V., Prager Straße 5,
99427 Weimar vertreten durch den Vorsitzenden Dr. Hans-Georg Timmler.

#### IHRE BETROFFENENRECHTE

Unter den angegebenen Kontaktdaten können Sie jederzeit folgende Rechte ausüben:

* Auskunft über Ihre bei uns gespeicherten Daten und deren Verarbeitung,
* Berichtigung unrichtiger personenbezogener Daten,
* Löschung Ihrer bei uns gespeicherten Daten,
* Einschränkung der Datenverarbeitung, sofern wir Ihre Daten aufgrund gesetzlicher Pflichten noch nicht löschen dürfen,
* Widerspruch gegen die Verarbeitung Ihrer Daten bei uns und
* Datenübertragbarkeit, sofern Sie in die Datenverarbeitung eingewilligt haben oder einen Vertrag mit uns abgeschlossen haben.

Sofern Sie uns eine Einwilligung erteilt haben, können Sie diese jederzeit mit
Wirkung für die Zukunft widerrufen.  
  
Sie können sich jederzeit mit einer Beschwerde an die für Sie zuständige
Aufsichtsbehörde wenden. Ihre zuständige Aufsichtsbehörde richtet sich nach
dem Bundesland Ihres Wohnsitzes, Ihrer Arbeit oder der mutmaßlichen Verletzung.
Eine Liste der Aufsichtsbehörden (für den nichtöffentlichen Bereich) mit Anschrift
finden Sie unter:
[https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html](https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html).

#### ZWECKE DER DATENVERARBEITUNG DURCH DIE VERANTWORTLICHE STELLE & DRITTE

Wir verarbeiten Ihre personenbezogenen Daten nur zu den in dieser
Datenschutzerklärung genannten Zwecken. Eine Übermittlung Ihrer persönlichen
Daten an Dritte zu anderen als den genannten Zwecken findet nicht statt. Wir
geben Ihre persönlichen Daten nur an Dritte weiter, wenn:

* Sie Ihre ausdrückliche Einwilligung dazu erteilt haben,
* die Verarbeitung zur Abwicklung eines Vertrags mit Ihnen erforderlich ist,
* die Verarbeitung zur Erfüllung einer rechtlichen Verpflichtung erforderlich ist,

die Verarbeitung zur Wahrung berechtigter Interessen erforderlich ist und kein
Grund zur Annahme besteht, dass Sie ein überwiegendes schutzwürdiges Interesse
an der Nichtweitergabe Ihrer Daten haben.

#### LÖSCHUNG BZW. SPERRUNG DER DATEN

Wir halten uns an die Grundsätze der Datenvermeidung und Datensparsamkeit.
Wir speichern Ihre personenbezogenen Daten daher nur so lange, wie dies zur
Erreichung der hier genannten Zwecke erforderlich ist oder wie es die vom
Gesetzgeber vorgesehenen vielfältigen Speicherfristen vorsehen. Nach Fortfall
des jeweiligen Zweckes bzw. Ablauf dieser Fristen werden die entsprechenden
Daten routinemäßig und entsprechend den gesetzlichen Vorschriften gesperrt
oder gelöscht.

#### SSL-VERSCHLÜSSELUNG

Um die Sicherheit Ihrer Daten bei der Übertragung zu schützen, verwenden wir dem
aktuellen Stand der Technik entsprechende Verschlüsselungsverfahren (z. B. SSL)
über HTTPS.

#### EMAIL-VERSCHLÜSSELUNG {#pgp}

Bitte denken Sie bei Mitteilungen per E-Mail daran, dass unverschlüsselt
übertragene Daten von Dritten eingesehen oder verändert werden können.  
Zur Übermittlung besonders sensibler Daten, wie bspw. Gesundheitsdaten, nutzen Sie bitte
einen unserer öffentlichen PGP-Schlüssel <a class="unstyled overlay symbol" href="/kontakt#teamleiter">🔒</a>
zur Ende-zu-Ende-Verschlüsselung ihrer Nachricht an die zugehörige E-Mail-Adresse.


Weitere Informationen zum Thema E-Mail-Verschlüsselung finden Sie unter:
[BSI: E-Mail Verschlüsselung](https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Informationen-und-Empfehlungen/Onlinekommunikation/Verschluesselt-kommunizieren/E-Mail-Verschluesselung/e-mail-verschluesselung.html)
und
[BSI: E-Mail Verschlüsselung in der Praxis](https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Informationen-und-Empfehlungen/Onlinekommunikation/Verschluesselt-kommunizieren/E-Mail-Verschluesselung/E-Mail-Verschluesselung-in-der-Praxis/e-mail-verschluesselung-in-der-praxis_node.html).

#### ÄNDERUNG UNSERER DATENSCHUTZBESTIMMUNGEN

Wir behalten uns vor, diese Datenschutzerklärung anzupassen, damit sie stets den
aktuellen rechtlichen Anforderungen entspricht oder um Änderungen unserer
Leistungen in der Datenschutzerklärung umzusetzen, z.B. bei der Einführung neuer
Services. Für Ihren erneuten Besuch gilt dann die neue Datenschutzerklärung.

#### FRAGEN ZUM DATENSCHUTZ

Wenn Sie Fragen zum Datenschutz haben, schreiben Sie uns bitte eine E-Mail oder
wenden Sie sich direkt an die Geschäftsstelle.

