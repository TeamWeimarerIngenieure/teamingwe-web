---
title: "Termine"
date: 2022-04-11T21:35:50+02:00
draft: false
toc: false
images:
tags:
  - rennen
  - events
---

Unsere Termine der Saison 2024 im Überblick:

## 1. BITBURGER 0.0% TRIATHLON-BUNDESLIGA

| Termin        | Ort       | Format |
|---------------|-----------|--------|
| 25. Mai       | Kraichgau | Sprint |
| 21. Juli      | Tübingen  | Sprint |
| 10. August    | Dresden   | Sprint |
| 07. September | Hannover  | Sprint |

## 2. TRIATHLON-BUNDESLIGA NORD

| Termin     | Ort                                                            | Format  |
|------------|----------------------------------------------------------------|---------|
| 25. Mai    | Freilingen                                                     | Sprint  |
| 09. Juni   | Eutin                                                          | Sprint  |
| 23. Juni   | [Kulturstadttriathlon Weimar](http://kulturstadttriathlon.de/) | Staffel |
| 04. August | Salzgitter                                                     | Sprint  |
| 18. August | Grimma                                                         | Sprint  |

## WEITERE EVENTS

| Termin      | Ort                                      | Veranstaltung    |
|-------------|------------------------------------------|------------------|
| 23. April   | Gewölbekeller der Stadtbibliothek Weimar | Teampräsentation |
| 21. Oktober | mon ami Weimar                           | Saisonabschluss  |
