---
title: "Team"
date: 2022-04-05T13:53:44+02:00
draft: false
toc: false
tags:
  - team
---

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">PAUL ADELT <a class="unstyled" href="/team#legend"><div class="league-count-1">0</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">1</div></a></h2>
        2008 <span class="flag">🇩🇪</span> TSV 1880 Gera-Zwötzen e.V.
        </td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Paul211.jpg" alt="Paul" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">HENRY BECK <a class="unstyled" href="/team#legend"><div class="league-count-1">1</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">4</div></a></h2>
1985 <span class="flag">🇩🇪</span> HSV Weimar e.V.
        </td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Henry177.jpg" alt="Henry" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">STRUAN BENNET <a class="unstyled" href="/team#legend"><div class="league-count-1">1</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">1</div></a></h2>
2004 <span class="flag">🏴󠁧󠁢󠁳󠁣󠁴󠁿</span> University of Stirling Triathlon Club
        </td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Struan6.jpg" alt="Struan" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">JULIUS DOMNICK <a class="unstyled" href="/team#legend"><div class="league-count-1">0</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">1</div></a></h2>
1998 <span class="flag">🇩🇪</span> Triathlon Jena e.V.
        </td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Julius00596.jpg" alt="Julius" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">TOBIAS DRACHLER <a class="unstyled" href="/team#legend"><div class="league-count-1">0</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">0</div></a></h2>
1990 <span class="flag">🇩🇪</span> Kölner Triathlon-Team 01
        </td>
        <td class="invisible" style="width: 25%">
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">RICHARD FEUER <a class="unstyled" href="/team#legend"><div class="league-count-1">7</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">3</div></a></h2>
2003 <span class="flag">🇩🇪</span> LTV Erfurt e.V.
</td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Richard00166.jpg" alt="Richard" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">LEON FISCHER <a class="unstyled" href="/team#legend"><div class="league-count-1">3</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">8</div></a></h2>
2004 <span class="flag">🇩🇪</span> HSV Weimar e.V.
        </td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Leon210.jpg" alt="Leon" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">THORBEN HÄDRICH <a class="unstyled" href="/team#legend"><div class="league-count-1">0</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">1</div></a></h2>
2006 <span class="flag">🇩🇪</span> HSV Weimar e.V.
</td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Thorben00383.jpg" alt="Thorben" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">MATTIS HASELBACH <a class="unstyled" href="/team#legend"><div class="league-count-1">0</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">1</div></a></h2>
2007 <span class="flag">🇩🇪</span> HSV Weimar e.V.
        </td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Mattis222.jpg" alt="Mattis" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">JOHN HEILAND <a class="unstyled" href="/team#legend"><div class="league-count-1">14</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">4</div></a></h2>
1990 <span class="flag">🇩🇪</span> SV Elbland Coswig-Meißen e.V.
        </td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/John188.jpg" alt="John" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">JONATHAN HEINEMANN <a class="unstyled" href="/team#legend"><div class="league-count-1">0</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">1</div></a></h2>
2008 <span class="flag">🇩🇪</span> HSV Weimar e.V.
</td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Jonathan00302.jpg" alt="Jonathan" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">ALEXANDER KULL <a class="unstyled" href="/team#legend"><div class="league-count-1">14</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">4</div></a></h2>
2000 <span class="flag">🇩🇪</span> HSV Weimar e.V.
</td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Alex00217.jpg" alt="Alex" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">PETER LEHMANN <a class="unstyled" href="/team#legend"><div class="league-count-1">1</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">11</div></a></h2>
1995 <span class="flag">🇩🇪</span> SV Elbland Coswig-Meißen e.V.
        </td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Peter170.jpg" alt="Peter" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">PHILIPP LEITERITZ <a class="unstyled" href="/team#legend"><div class="league-count-1">0</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">8</div></a></h2>
1998 <span class="flag">🇩🇪</span> Triathlon Jena e.V.
</td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Philipp00462.jpg" alt="Philipp" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">PHILIPP MACK <a class="unstyled" href="/team#legend"><div class="league-count-1">4</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">1</div></a></h2>
2003 <span class="flag">🇩🇪</span> SSV Ulm 1846 e.V.
        </td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/PhilippM00419.jpg" alt="Philipp" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">PHIL PFEIFER <a class="unstyled" href="/team#legend"><div class="league-count-1">1</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">1</div></a></h2>
2008 <span class="flag">🇩🇪</span> LTV Erfurt e.V.
</td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Phil00336.jpg" alt="Phil" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">STIG RUDOLPH <a class="unstyled" href="/team#legend"><div class="league-count-1">0</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">2</div></a></h2>
2008 <span class="flag">🇩🇪</span> TSV 1880 Gera-Zwötzen e.V.
</td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Stig00260.jpg" alt="Stig" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">HANNES RUHNKE <a class="unstyled" href="/team#legend"><div class="league-count-1">0</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">1</div></a></h2>
2008 <span class="flag">🇩🇪</span> TSV 1880 Gera-Zwötzen e.V.
        </td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Hannes220.jpg" alt="Hannes" />
        </td>
    </tr>
</table>

<table class="invisible no-margin" style="margin: 25px 0">
    <tr class="invisible">
        <td class="invisible" style="width: 75%">
        <h2 style="margin-top: 0">THEO SONNENBERG <a class="unstyled" href="/team#legend"><div class="league-count-1">15</div></a><a class="unstyled" href="/team#legend"><div class="league-count-2">5</div></a></h2>
2001 <span class="flag">🇩🇪</span> Triathlon Jena e.V.
</td>
        <td class="invisible" style="width: 25%">
            <img class="avatar" src="/img/Theo00555.jpg" alt="Theo" />
        </td>
    </tr>
</table>

##
#### * STARTS SEIT 2019 <span class="league-count-1">1. Liga</span><span class="league-count-2">2. Liga</span> {#legend}
