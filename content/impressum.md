---
title: "Impressum"
date: 2022-04-11T20:29:48+02:00
draft: false
toc: false
images:
tags:
---

Inhaltlich verantwortlich i.S.d.P.: **HSV Weimar e.V., Abteilung Triathlon**  
Sitz Weimar

#### POSTANSCHRIFT

HSV Weimar e.V.

Geschäftsstelle  
Prager Straße 5  
99427 Weimar  

Tel.: +49 (3643) 901431  
Fax: +49 (3643) 901431  
E-Mail: info@hsv-weimar.de  

Weitere Informationen unter [www.hsv-weimar.de](https://www.hsv-weimar.de) bzw.
[www.hsv-weimar-triathlon.de](http://www.hsv-weimar-triathlon.de).

Bitte denken Sie bei Mitteilungen per e-mail daran, dass unverschlüsselt übertragene Daten
von Dritten eingesehen oder verändert werden können.

#### REGISTEREINTRAGUNG / VERTRETUNGSBERECHTIGUNG

Der HSV Weimar e.V. ist im Vereinsregister des Amtgerichts Weimar unter VR 109 eingetragen.
Der HSV Weimar wird durch den 1. Vorsitzenden Dr. Hans-Georg Timmler, den 2. Vorsitzenden
Matthias Stieff und den Schatzmeister Henry Mai gemäß § 8 der Satzung vertreten.

Steuernummer: 151 / 141 / 60676

#### HAFTUNG

Für die Vollständigkeit und Richtigkeit der eingestellten Informationen kann keine Gewähr übernommen werden. Für die eigenen Inhalte ist der HSV Weimar e.V. als Inhaltsanbieter nach allgemeinen Gesetzen verantwortlich. Durch dynamische Querverweise (“Links”) können Sie auf die Seiten anderer Anbieter gelangen. Der HSV Weimar e.V. kann keine Haftung für den Inhalt dieser Seiten übernehmen.

