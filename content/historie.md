---
title: "Historie"
date: 2024-02-03T14:05:23+01:00
draft: false
toc: false
images:
tags:
---

![Mannschaft](/img/2022-09-03-Hannover-Team-Ziel.jpg)

Mit der Einführung des Thüringer Nachwuchskadersystems und der Entstehung von ersten
[Juniorteams](http://www.hsv-weimar-triathlon.de/mannschaften/juniorteam/) innerhalb
der Vereine wurde der Grundstein zur Förderung des Nachwuchsleistungssports im Freistaat
geschaffen.
Als durchaus problematische Entwicklung stellte sich der vermehrte Weggang talentierter
Sportler heraus, die mit Verlassen des Juniorenalters in Vereine und Liga-Mannschaften
anderer Bundesländer wechselten. Um diesem Phänomen zu begegnen und talentierten Triathleten
auch über die Jugend hinaus eine leistungssportlich attraktive Perspektive in der Region Weimar
zu bieten, wurde das **TEAM WEIMARER INGENIEURE** zunächst als Regionalliga-Mannschaft gegründet.
Das mittlerweile in der Bundesliga etablierte **TEAM WEIMARER INGENIEURE** gehört beständig zu
den Medaillenkandidaten im Liga-Betrieb und zieht Sportler auch über die Landesgrenzen hinaus an.
Durch eine sehr enge Zusammenarbeit mit dem
[Thüringer Triathlon Verband](https://https://thueringer-triathlon-verband.de/) und dem Thüringer
Nachwuchskader werden Synergien zwischen jungen und erfahrenen Sportlern sowie Funktionären
bestmöglich genutzt.


## SPORTLICHE ENTWICKLUNG

Als Sieger der [Regionalliga Ost](https://www.triathlon-regionalliga.de/) im Jahr 2009 sicherte
sich die Mannschaft die Aufstiegsberechtigung in die
[2. Triathlon-Bundesliga Nord](https://www.triathlonbundesliga.de/2-bundesliga). In den
folgenden Jahren gelang es dem Team sich im Mittelfeld des Gesamtklassements zu etablieren, bevor
2013 mit Platz 4 der Anschluss an die erweiterte Liga-Spitze hergestellt werden konnte. Bereits
zwei Jahre später konnte das TEAM WEIMARER INGENIEURE mit dem dritten Rang erstmalig einen
Podestplatz feiern. Seitdem gehört die Mannschaft beständig zu den Medaillenkandidaten der
2. Bundesliga Nord. Auch die Saison 2018 beendete die Mannschaft als Vizemeister der
2. Triathlon-Bundesliga Nord erfolgreich. Mit diesem Ergebnis sicherte sich das
**TEAM WEIMARER INGENIEURE** erstmalig das Aufstiegsrecht in die
[1. Bitburger 0.0% Triathlon-Bundesliga](http://www.triathlonbundesliga.de/). Nach einer bewegten Saison und dem
15. Platz in der Ligawertung konnte der Klassenerhalt nicht geschafft werden. Im Jahr 2021 schickte
das **TEAM WEIMARER INGENIEURE** erstmalig zwei Mannschaften ins Rennen: Neben der etablierten
Zweitligamannschaft wurde eine Regionalligamannschaft gegründet, deren Betätigungsfeld wie bereits
2009 die Regionalliga Ost sein würde. Mit dem ambitionierten Ziel den Aufstieg mit beiden
Mannschaften zu erringen, startete das **TEAM WEIMARER INGENIEURE** beherzt in die Wettkampfsaison
2021. Als Sieger sowohl der 2. Triathlon-Bundesliga Nord als auch der Regionalliga Ost 
gehen die Mannschaften im Jahr 2022 in Idealkonstellation an den Saisonstart der 1. Triathlon
Bundesliga und der 2. Triathlon-Bundesliga Nord.
